/*
 * NetworkInterface.c
 *
 *  Created on: Jun 14, 2018
 *      Author: Erik Trauschke
 */

/*
 * Based on FreeRTOS+TCP V2.0.1 LPC18xx network driver.
 * Copyright (C) 2017 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 */

/*
 * Copyright (C) 2018 Monterey Bay Aquarium Research Institute
 * All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * 1 tab == 4 spaces!
 */


/* Standard includes. */
#include <stdint.h>
#include <string.h>

/* LPCOpen includes */
#include "board.h"
#include "lpc_phy.h"

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* FreeRTOS+TCP includes. */
#include "FreeRTOS_IP.h"
#include "FreeRTOS_IP_Private.h"
#include "NetworkBufferManagement.h"

#include "NetworkInterface.h"

/*
 * Set default values for configuration parameters.
 */

#ifndef configNUM_TX_DESCRIPTORS
	#define configNUM_TX_DESCRIPTORS 3
#endif

#ifndef configNUM_RX_DESCRIPTORS
	#define configNUM_RX_DESCRIPTORS 4
#endif

#ifndef configRX_PRIORITY_BIAS
	#define configRX_PRIORITY_BIAS	3
#endif

#ifndef configPHY_CHECK_TIME
	#define configPHY_CHECK_TIME	500UL
#endif


#define	EMAC_EVT_RX		1
#define EMAC_EVT_TX		2

static ENET_RXDESC_T *pxRXDescs;
static ENET_RXSTAT_T *pxRXStats;
static ENET_TXDESC_T *pxTXDescs;
static ENET_TXSTAT_T *pxTXStats;

static TaskHandle_t xRxHandlerTask = NULL;
NetworkBufferDescriptor_t *xTxNetBufs[configNUM_TX_DESCRIPTORS];
static SemaphoreHandle_t xTXDescriptorSemaphore = NULL;
static uint16_t usRxConsumeIdx;
static uint16_t usTxProduceIdx;
static volatile uint8_t ucIntEvents = 0;

/* MAC address to be used needs to be specified in application */ 
extern const uint8_t ucMACAddress[6];

static int globci = -1;

int sent = 0;
int cleared = 0;
int txdone = 0;
int rxdone = 0;
int txur = 0;
int txfin = 0;
int txerr = 0;
int txloops = 0;
size_t freeheap;

static void prvEMACHandlerTask( void *pvParameters );
/*-----------------------------------------------------------*/

static int prvSetTXBuffer( void *xBuffer )
{
    uint16_t usTxConsumeIdx = Chip_ENET_GetTXConsumeIndex( LPC_ETHERNET );

    if( Chip_ENET_GetBufferStatus( LPC_ETHERNET, usTxProduceIdx, usTxConsumeIdx,
	  configNUM_TX_DESCRIPTORS ) != ENET_BUFF_FULL ) {
        pxTXDescs[ usTxProduceIdx ].Packet = ( uint32_t )xBuffer;
        return true;
    }
    return false;
}
/*-----------------------------------------------------------*/

static void prvCheckPHY()
{
	uint32_t ulPhyStatus;
	static uint32_t ulOldPhyStatus = 0;

	ulPhyStatus = lpcPHYStsPoll();

	if( !( ulPhyStatus & PHY_LINK_CONNECTED ) ) return;
	if( ulOldPhyStatus == ulPhyStatus ) return;

	ulOldPhyStatus = ulPhyStatus;

	if( ulPhyStatus & PHY_LINK_SPEED100 )
		Chip_ENET_Set100Mbps( LPC_ETHERNET );
	else
		Chip_ENET_Set10Mbps( LPC_ETHERNET );

	if( ulPhyStatus & PHY_LINK_FULLDUPLX )
		Chip_ENET_SetFullDuplex( LPC_ETHERNET );
	else
		Chip_ENET_SetHalfDuplex( LPC_ETHERNET );
}
/*-----------------------------------------------------------*/

void emac_msleep(uint32_t ms)
{

	/*
	 * One for loop is 6 - 8 instructions depending on optimization,
	 * but we're not doing high precision timing here.
	 */
    int div = SystemCoreClock / 1000000 / 7;
    volatile uint32_t i, count = ms * 1000;

    while( --count > 0 )
	{
        for( i = 0; i < div; i++ )
		{
        	/* make sure for-loop doesn't get optimized out by compiler */
        	__ASM volatile ("nop");

        }
	}
}
/*-----------------------------------------------------------*/

/* Initialize MAC descriptors for simple packet receive/transmit */
static void 
prvInitDescriptors()
{
    int i;
    const TickType_t xDescriptorWaitTime = pdMS_TO_TICKS( 250 );
    NetworkBufferDescriptor_t *pxDescriptor;
	static uint8_t ucDescInit = 0;

	if( ucDescInit ) return;

	/*
	 * The descriptors need to be 8 byte aligned but pvPortMalloc will take
	 * care of that. portBYTE_ALIGNMENT for ARM_CM3 is 8.
	 */
    pxTXDescs = pvPortMalloc( configNUM_TX_DESCRIPTORS * sizeof( ENET_TXDESC_T ) );
    pxTXStats = pvPortMalloc( configNUM_TX_DESCRIPTORS * sizeof( ENET_TXSTAT_T ) );
    pxRXDescs = pvPortMalloc( configNUM_RX_DESCRIPTORS * sizeof( ENET_RXDESC_T ) );
    pxRXStats = pvPortMalloc( configNUM_RX_DESCRIPTORS * sizeof( ENET_RXSTAT_T ) );
    configASSERT( pxTXDescs && pxTXStats && pxRXDescs && pxRXStats );

    memset( pxTXDescs, 0, configNUM_TX_DESCRIPTORS * sizeof(ENET_TXDESC_T) );
    memset( pxTXStats, 0, configNUM_TX_DESCRIPTORS * sizeof(ENET_TXSTAT_T) );
    memset( pxRXDescs, 0, configNUM_RX_DESCRIPTORS * sizeof(ENET_RXDESC_T) );
    memset( pxRXStats, 0, configNUM_RX_DESCRIPTORS * sizeof(ENET_RXSTAT_T) );

    usRxConsumeIdx = 0;
    usTxProduceIdx = 0;

	/* Assign network buffers for each RX descriptor */
	for( i = 0; i < configNUM_RX_DESCRIPTORS; i++ )
	{
		pxDescriptor = pxGetNetworkBufferWithDescriptor( ipconfigNETWORK_MTU,
		  xDescriptorWaitTime );
		configASSERT( pxDescriptor );

		pxRXDescs[ i ].Packet = ( uint32_t )pxDescriptor->pucEthernetBuffer;
		pxRXDescs[ i ].Control = ENET_RCTRL_INT | ENET_RCTRL_SIZE(
		  ipconfigNETWORK_MTU );
		pxRXStats[ i ].StatusInfo = 0;
		pxRXStats[ i ].StatusHashCRC = 0;
	}

	/* This is a zero copy driver, so no need to assign buffers for TX */
	for( i = 0; i < configNUM_TX_DESCRIPTORS; i++ )
	{
		pxTXDescs[ i ].Packet = 0xdeadbeef;
		pxTXDescs[ i ].Control = 0;
		pxTXStats[ i ].StatusInfo = 0;
	}

	Chip_ENET_InitTxDescriptors( LPC_ETHERNET, pxTXDescs, pxTXStats,
	  configNUM_TX_DESCRIPTORS );
	Chip_ENET_InitRxDescriptors( LPC_ETHERNET, pxRXDescs, pxRXStats,
	  configNUM_RX_DESCRIPTORS );

	ucDescInit = 1;
}
/*-----------------------------------------------------------*/

BaseType_t xNetworkInterfaceInitialise( void )
{
	BaseType_t xReturn = pdPASS;

	Chip_ENET_Init( LPC_ETHERNET, true );
	Chip_ENET_SetupMII( LPC_ETHERNET, Chip_ENET_FindMIIDiv( LPC_ETHERNET,
	  2500000 ), 1 );
	Chip_ENET_SetADDR( LPC_ETHERNET, ucMACAddress );

	( void )lpc_phy_init( true, emac_msleep, 0, 0 );

	prvInitDescriptors();

  	/* Enable RX/TX after descriptors are setup */
	Chip_ENET_TXEnable( LPC_ETHERNET );
	Chip_ENET_RXEnable( LPC_ETHERNET );

	/*
	 * Enable hardware filtering for unicast and broadcast packets. This should
	 * make the use of ipCONSIDER_FRAME_FOR_PROCESSING superfluous.
	 */
	Chip_ENET_EnableRXFilter( LPC_ETHERNET,
	  ENET_RXFILTERCTRL_APE | ENET_RXFILTERCTRL_ABE );

	xReturn = xTaskCreate( prvEMACHandlerTask, "EMAC", configMINIMAL_STACK_SIZE,
	  NULL, configMAX_PRIORITIES - 1, &xRxHandlerTask );
	configASSERT( xReturn );

	if( xTXDescriptorSemaphore == NULL )
	{
		/*
		 * Create counting semaphore for TX descriptors. Since we can not
		 * distinguish between a completely full or empty set of TX descriptors
		 * with consume and produce index alone there always needs to be one
		 * more descriptor than we can have loaded.
		 * Even though there would be ways around this in software we can't
		 * load more descriptors safely given the way the hardware manages the
		 * descriptors.
		 */
		xTXDescriptorSemaphore = xSemaphoreCreateCounting(
		  ( UBaseType_t ) configNUM_TX_DESCRIPTORS - 1,
		  ( UBaseType_t ) configNUM_TX_DESCRIPTORS - 1);
		configASSERT( xTXDescriptorSemaphore );
	}

	/*
	 * Check if the PHY is up and adjust speed/duplex. If not, this will be run
	 * periodically again.
	 */
	prvCheckPHY();

	return xReturn;
}
/*-----------------------------------------------------------*/

BaseType_t xNetworkInterfaceOutput( NetworkBufferDescriptor_t * const pxDescriptor,
                                    BaseType_t xReleaseAfterSend )
{
	BaseType_t xReturn = pdFAIL;
	TickType_t xBlockTimeTicks = pdMS_TO_TICKS(100);
	TimeOut_t xTimeOut;
	bool nobuf = false;
	bool sentout = false;

	do 
	{
		if( !pxDescriptor->xDataLength || !xTXDescriptorSemaphore ) 
		{
			break;
		}
		if( xSemaphoreTake( xTXDescriptorSemaphore, xBlockTimeTicks ) != pdPASS )
		{
			/* Time-out waiting for a free TX descriptor. */
			break;
		}

		vTaskSetTimeOutState( &xTimeOut );	
		while ( prvSetTXBuffer( pxDescriptor->pucEthernetBuffer ) == false )
		{
			if( xTaskCheckForTimeOut( &xTimeOut, &xBlockTimeTicks ) != pdFALSE )
			{
				/* We couldn't get free descriptor. */
				nobuf = true;
				break;
			}
		}
		if ( nobuf )
		{
			xSemaphoreGive( xTXDescriptorSemaphore );
			break;
		}

		/*
		 * Remember which descriptor index the network buffer was assigned to
		 * so that we can release it after it got sent out.
		 */
		if ( xReleaseAfterSend != pdFALSE )
		{
			xTxNetBufs[ Chip_ENET_GetTXProduceIndex( LPC_ETHERNET ) ] = \
			  pxDescriptor;
		}

		pxTXDescs[usTxProduceIdx].Control = \
		  ENET_TCTRL_SIZE( pxDescriptor->xDataLength ) | ENET_TCTRL_LAST | \
		  ENET_TCTRL_INT;
		usTxProduceIdx = Chip_ENET_IncTXProduceIndex( LPC_ETHERNET );

		sent++;
		sentout = true;
		xReturn = pdPASS;

		iptraceNETWORK_INTERFACE_TRANSMIT();
	} 
	while( 0 );

	if( !sentout && xReleaseAfterSend != pdFALSE )
	{
		/*
		 * We had some problem sending paket out, make sure we release the
		 * network buffer if required
		 */
		vReleaseNetworkBufferAndDescriptor( pxDescriptor );
	}

    return xReturn;
}
/*-----------------------------------------------------------*/

static __inline void prvNetworkInterfaceInput()
{
	uint8_t *pucRxBuf;
	uint32_t ulRxBytes, ulStatus;
	NetworkBufferDescriptor_t *pxDescriptor, *pxNewDescriptor;
	IPStackEvent_t xRxEvent = { eNetworkRxEvent, NULL };
	const TickType_t xDescriptorWaitTime = pdMS_TO_TICKS( 250 );
	uint8_t ucRxCount = 0;
	uint16_t usRxProduceIdx;

	/*
	 * If there is a lot of traffic we might be stuck in this loop for a while.
	 * So we yield when we went through a few rounds to make sure we
	 * can still send out packets. However, when we grant CPU time for TX we
	 * have to make sure we come back here to finish up because we might not get
	 * another interrupt to trigger this code path.
	 */
	while( ucRxCount++ < configRX_PRIORITY_BIAS )
	{
		usRxProduceIdx = Chip_ENET_GetRXProduceIndex( LPC_ETHERNET );

		if ( Chip_ENET_GetBufferStatus( LPC_ETHERNET, usRxProduceIdx,
		  usRxConsumeIdx, configNUM_RX_DESCRIPTORS ) != ENET_BUFF_EMPTY )
		{
			pucRxBuf = ( void * ) pxRXDescs[ usRxConsumeIdx ].Packet;
			ulStatus = pxRXStats[ usRxConsumeIdx ].StatusInfo;
			/* Remove CRC */
			ulRxBytes = ( uint32_t ) ( ENET_RINFO_SIZE( ulStatus ) - 4);
		}
		else 
		{
			/*
			 * Only clear RX event flag when there is nothing to do anymore
			 * to make sure we come back here after we did TX.
			 */
			ucIntEvents &= ~EMAC_EVT_RX;
			break;
		}
		/*
		 * Check for errors.
		 * Note that range errors are not indicators for an error per se since
		 * it just checks the length field in the ethernet header which usually
		 * is used as a type indicator.
		 */
		if(
		  !ulRxBytes ||
		  ( ulStatus & ENET_RINFO_ERR && !( ulStatus & ENET_RINFO_RANGE_ERR ) )
		  )
		{
			usRxConsumeIdx = Chip_ENET_IncRXConsumeIndex( LPC_ETHERNET );
			continue;
		}

		/*
		 * Get new network buffer to replace the one which just received a
		 * packet. If we can not get one, drop the packet and keep the old
		 * buffer in place.
		 */
		pxNewDescriptor = pxGetNetworkBufferWithDescriptor( ipconfigNETWORK_MTU,
		  xDescriptorWaitTime );
		if ( !pxNewDescriptor )
		{
			usRxConsumeIdx = Chip_ENET_IncRXConsumeIndex( LPC_ETHERNET );
			continue;
		}

		pxDescriptor = pxPacketBuffer_to_NetworkBuffer( pucRxBuf );
		pxDescriptor->xDataLength = ulRxBytes;
		pxRXDescs[ usRxConsumeIdx ].Packet = \
		  ( uint32_t )pxNewDescriptor->pucEthernetBuffer;

		xRxEvent.pvData = (void *)pxDescriptor;
		if( xSendEventStructToIPTask( &xRxEvent, ( TickType_t ) 0 ) == pdFAIL)
		{
			vReleaseNetworkBufferAndDescriptor( pxDescriptor );
			iptraceETHERNET_RX_EVENT_LOST();
		}
		usRxConsumeIdx = Chip_ENET_IncRXConsumeIndex( LPC_ETHERNET );
	}
}
/*-----------------------------------------------------------*/

static __inline void prvReleaseTXDesc()
{
	static uint8_t ucTxNetBufToFree = 0;

	while( 1 )
	{
		if( ucTxNetBufToFree != Chip_ENET_GetTXConsumeIndex( LPC_ETHERNET ) )
		{ 
			if( xTxNetBufs[ ucTxNetBufToFree ] != NULL )
			{
				vReleaseNetworkBufferAndDescriptor(
				  xTxNetBufs[ ucTxNetBufToFree ] );
				xTxNetBufs[ ucTxNetBufToFree ] = NULL;
			}
			xSemaphoreGive( xTXDescriptorSemaphore );
			cleared++;
			if ( ++ucTxNetBufToFree >= configNUM_TX_DESCRIPTORS ) ucTxNetBufToFree = 0;
		}
		else
		{
			break;
		}
	}
}
/*-----------------------------------------------------------*/

static void prvEMACHandlerTask( void *pvParameters )
{
	const TickType_t xBlockTime = pdMS_TO_TICKS( 500ul );
	const TickType_t xPhyCheckInterval = pdMS_TO_TICKS( configPHY_CHECK_TIME );
	static TickType_t xNextPhyCheck = 0;
	TickType_t xCurrentTick;

	/* 
	 * We have to turn on interrupts here to make sure we are ready
	 * to receive notifications from ulTaskNotify...
	 */
	Chip_ENET_EnableInt( LPC_ETHERNET, ~0 );
	NVIC_SetPriority( ETHERNET_IRQn, configMAC_INTERRUPT_PRIORITY );
	NVIC_EnableIRQ( ETHERNET_IRQn );

	for( ;; )
	{
		freeheap = xPortGetFreeHeapSize();
		globci = Chip_ENET_GetTXConsumeIndex( LPC_ETHERNET );

		ulTaskNotifyTake( pdTRUE,  xBlockTime );
		while ( ucIntEvents )
		{
			if ( ucIntEvents & EMAC_EVT_RX )
			{
				prvNetworkInterfaceInput();
			}
			if ( ucIntEvents & EMAC_EVT_TX )
			{
				prvReleaseTXDesc();
				ucIntEvents &= ~EMAC_EVT_TX;
			}
		}

		/* Check if the PHY status has changed */
		xCurrentTick = xTaskGetTickCount();
		if( xCurrentTick > xNextPhyCheck )
		{
			xNextPhyCheck = xCurrentTick + xPhyCheckInterval;
			prvCheckPHY();
		}
	}
}
/*-----------------------------------------------------------*/

void ETH_IRQHandler( void )
{
	uint32_t ulIntStatus = 1;
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	while( ( ulIntStatus = Chip_ENET_GetIntStatus( LPC_ETHERNET ) ) != 0 )
	{
		/* Clear interrupts. */
		Chip_ENET_ClearIntStatus( LPC_ETHERNET, ulIntStatus );

		if( ulIntStatus & ENET_INT_RXDONE )
		{
			rxdone++;
			ucIntEvents |= EMAC_EVT_RX;
			vTaskNotifyGiveFromISR( xRxHandlerTask, &xHigherPriorityTaskWoken );
		}
		if( ulIntStatus & ENET_INT_TXDONE )
		{
			txdone++;
			ucIntEvents |= EMAC_EVT_TX;
			vTaskNotifyGiveFromISR( xRxHandlerTask, &xHigherPriorityTaskWoken );
		}
		if ( ulIntStatus & ENET_INT_RXOVERRUN )
		{
			Chip_ENET_ResetRXLogic( LPC_ETHERNET );
			usRxConsumeIdx = 0;
		}
		if ( ulIntStatus & ENET_INT_TXUNDERRUN )
		{
			txur++;
			Chip_ENET_ResetTXLogic( LPC_ETHERNET );
			usTxProduceIdx = 0;
		}
	}
	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}

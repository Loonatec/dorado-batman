configPLACE_IN_SECTION_RAM
static BaseType_t prvNetworkInterfaceInput()
{
BaseType_t xResult = pdFALSE;
uint32_t ulStatus;
eFrameProcessingResult_t eResult;
const TickType_t xDescriptorWaitTime = pdMS_TO_TICKS( 250 );
const UBaseType_t uxMinimumBuffersRemaining = 3UL;
uint16_t usLength;
NetworkBufferDescriptor_t *pxDescriptor;
#if( ipconfigZERO_COPY_RX_DRIVER != 0 )
	NetworkBufferDescriptor_t *pxNewDescriptor;
#endif /* ipconfigZERO_COPY_RX_DRIVER */
IPStackEvent_t xRxEvent = { eNetworkRxEvent, NULL };

	/* Process each descriptor that is not still in use by the DMA. */
	ulStatus = xDMARxDescriptors[ ulNextRxDescriptorToProcess ].STATUS;
	if( ( ulStatus & RDES_OWN ) == 0 )
	{
		/* Check packet for errors */
		if( ( ulStatus & nwRX_STATUS_ERROR_BITS ) != 0 )
		{
			/* There is some reception error. */
			/* Clear error bits. */
			ulStatus &= ~( ( uint32_t )nwRX_STATUS_ERROR_BITS );
		}
		else
		{
			xResult++;

			eResult = ipCONSIDER_FRAME_FOR_PROCESSING( ( const uint8_t * const ) ( xDMARxDescriptors[ ulNextRxDescriptorToProcess ].B1ADD ) );
			if( eResult == eProcessBuffer )
			{
				if( ( ulPHYLinkStatus & PHY_LINK_CONNECTED ) == 0 )
				{
					ulPHYLinkStatus |= PHY_LINK_CONNECTED;
					FreeRTOS_printf( ( "prvEMACHandlerTask: PHY LS now %d (message received)\n", ( ulPHYLinkStatus & PHY_LINK_CONNECTED ) != 0 ) );
				}

				if( uxGetNumberOfFreeNetworkBuffers() > uxMinimumBuffersRemaining )
				{
					pxNewDescriptor = pxGetNetworkBufferWithDescriptor( ipTOTAL_ETHERNET_FRAME_SIZE, xDescriptorWaitTime );
				}
				else
				{
					/* Too risky to allocate a new Network Buffer. */
					pxNewDescriptor = NULL;
				}
				if( pxNewDescriptor != NULL )
				{
				const uint8_t *pucBuffer;

					/* Get the actual length. */
					usLength = RDES_FLMSK( ulStatus );

					{
						/* Replace the character buffer 'B1ADD'. */
						pucBuffer = ( const uint8_t * const ) ( xDMARxDescriptors[ ulNextRxDescriptorToProcess ].B1ADD );
						xDMARxDescriptors[ ulNextRxDescriptorToProcess ].B1ADD = ( uint32_t ) pxNewDescriptor->pucEthernetBuffer;

						/* 'B1ADD' contained the address of a 'pucEthernetBuffer' that
						belongs to a Network Buffer.  Find the original Network Buffer. */
						pxDescriptor = pxPacketBuffer_to_NetworkBuffer( pucBuffer );
						/* This zero-copy driver makes sure that every 'xDMARxDescriptors' contains
						a reference to a Network Buffer at any time.
						In case it runs out of Network Buffers, a DMA buffer won't be replaced,
						and the received messages is dropped. */
						configASSERT( pxDescriptor != NULL );
					}

					if( pxDescriptor != NULL )
					{
						pxDescriptor->xDataLength = ( size_t ) usLength;
						/* It is possible that more data was copied than
						actually makes up the frame.  If this is the case
						adjust the length to remove any trailing bytes. */
						prvRemoveTrailingBytes( pxDescriptor );

						/* Pass the data to the TCP/IP task for processing. */
						xRxEvent.pvData = ( void * ) pxDescriptor;
						if( xSendEventStructToIPTask( &xRxEvent, xDescriptorWaitTime ) == pdFALSE )
						{
							/* Could not send the descriptor into the TCP/IP
							stack, it must be released. */
							vReleaseNetworkBufferAndDescriptor( pxDescriptor );
						}
						else
						{
							iptraceNETWORK_INTERFACE_RECEIVE();
						}
					}
				}
			}
			/* Got here because received data was sent to the IP task or the
			data contained an error and was discarded.  Give the descriptor
			back to the DMA. */
			xDMARxDescriptors[ ulNextRxDescriptorToProcess ].STATUS = ulStatus | RDES_OWN;

			/* Move onto the next descriptor. */
			ulNextRxDescriptorToProcess++;
			if( ulNextRxDescriptorToProcess >= configNUM_RX_DESCRIPTORS )
			{
				ulNextRxDescriptorToProcess = 0;
			}

			ulStatus = xDMARxDescriptors[ ulNextRxDescriptorToProcess ].STATUS;
		} /* if( ( ulStatus & nwRX_STATUS_ERROR_BITS ) != 0 ) */
	} /* if( ( ulStatus & RDES_OWN ) == 0 ) */

	/* Restart receive polling. */
	LPC_ETHERNET->DMA_REC_POLL_DEMAND = 1;

	return xResult;
}

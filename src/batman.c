/****************************************************************************/
/* Copyright (c) 2020 MBARI                                                 */
/* MBARI Proprietary Information. All rights reserved.                      */
/****************************************************************************/
/* Summary  :                                                               */
/* Filename : batman.c                                                      */
/* Author   :                                                               */
/* Project  :                                                               */
/* Version  : 1.0                                                           */
/* Created  : 2019                                                          */
/* Modified :                                                               */
/* Archived :                                                               */
/****************************************************************************/
/* Modification History:                                                    */
/****************************************************************************/

/* 
 * AUV Battery Management Server (BatMan)
 *
 *
 *               ++
 *               ||
 *               ||
 *   --     .----++-------------+----------O---------+-----------------+---.
 *    )\  /                     |                    |                 |    \
 *    >-<|                      |        MBARI       |                 |    |
 *   ( /  \                     |                    |                 |    /
 *   --     `-------------------+--------------------+-----------------+---'
 *
 * 
 *
 */

#include "board.h"
#include "iap_driver.h"
#include "indicators.h"
#include "uart.h"
#include "smbus.h"
#include "srv_ui.h"
#include "utils.h"

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "udp_types.h"
#include "net.h"

#include "sensors.h"
#include "external_eeprom.h"
#include "rtc_time.h"

#define STR_BUFF_SIZE 3000	//@99CV gives approx 2600 bytes, including \r\n

uint8_t ucMACAddress[]			= { 0x00, 0x0c, 0x6a, 0x01, 0x03, 0x10 };
char tcp_srv_ip[16]			= "10.0.0.2";		/* IP address */
char tcp_srv_gw[16]  		= "10.0.0.1";		/* gateway */
char tcp_srv_nm[16] 		= "255.255.255.0";	/* netmask */
char udp_default_dest[16]	= "10.0.0.5";		/* default destination for UDP packets, only used if needs to send packet w/o receiving request */

const uint16_t net_tcp_buffsize = 256;

const uint16_t tcp_base_port = 10000;
const uint16_t udp_base_port = 20000;
extern Socket_t default_udp_sock;
extern BALL_DATA_892_T bdata_892;

void panic(char *msg);

xSemaphoreHandle bat_semphr;
xSemaphoreHandle comms_semphr;
char str_data[STR_BUFF_SIZE];

APP_CFG control_app;
APP_CFG battery_app;

CONTROL_CFG control_cfg;
BATTERY_CFG battery_cfg;

TASK_CFG maintask;

/* minimum free heap to accept a new connection, 8kB */
#define TCP_MIN_FREE_HEAP		(8192)

const char version[] = "3.2.6-b1";

	/* Amount of time to delay between updating battery status in seconds.
	 *   Note: inactive packs go into a standby mode after 10 seconds of SMBus silence */
#define BATT_UPDATE_DELAY_S 600

#define WATCHDOG_ON			1		// Enable internal chip's WDT
#define WATCHDOG_TIMEOUT	5		// In seconds, feed by heart-beat indicator function
bool wdt_running = false;

/* bypass store/load of variables into flash for debug reasons */
#define FLASH_BYPASS	0

#if defined(ENABLE_UART_BRIDGES)
TASK_CFG serialtask;
xTaskHandle to_ts_sercomm = NULL;
#endif

/* global task handles */
xTaskHandle to_ts_comm = NULL;
xTaskHandle to_ts_battery = NULL;

FLASH_CFG flash_cfg;

/* local functions */
static void commTask(void *parm);

void init_wdt(uint32_t timeout);
int send_to_uart(void *arg, int sockidx, int len, void *unused);
void decode_ip(char *ip, char *ipstr);
int load_flash_config(void);
void store_flash_config(void);


void
init_wdt(uint32_t timeout) {

	uint32_t wdtFreq;
	/*	Initialize WWDT and event router */
	Chip_WWDT_Init(LPC_WWDT);
	Chip_WWDT_SelClockSource(LPC_WWDT, WWDT_CLKSRC_WATCHDOG_PCLK);
	wdtFreq = Chip_Clock_GetPeripheralClockRate(SYSCTL_PCLK_WDT) / 4;

	/* Set watchdog feed time constant to timeout */
	Chip_WWDT_SetTimeOut(LPC_WWDT, wdtFreq * timeout);

	/* Configure WWDT to reset on timeout */
	Chip_WWDT_SetOption(LPC_WWDT, WWDT_WDMOD_WDEN | WWDT_WDMOD_WDRESET);

	/* Clear watchdog warning and timeout interrupts */
	Chip_WWDT_ClearStatusFlag(LPC_WWDT, WWDT_WDMOD_WDTOF | WWDT_WDMOD_WDINT);

	/* Start watchdog */
	Chip_WWDT_Start(LPC_WWDT);

	/* Enable watchdog interrupt */
	NVIC_ClearPendingIRQ(WDT_IRQn);
	NVIC_EnableIRQ(WDT_IRQn);
	wdt_running = true;
}

int
handle_ctl_requests(void *arg, int sockidx, int len, void *dest)
{
	struct freertos_sockaddr *client = (struct freertos_sockaddr *)dest;
	APP_CFG *app = (APP_CFG *)arg;
	CONTROL_CFG *ctlcfg = (CONTROL_CFG *)app->ext_app_cfg;
	Socket_t sock = app->clientsocks[sockidx];
	char response[32];

	int ret;

	ret = parse_ctl_cmd(app, sockidx, len, client);
	if (ret) return (ret);
	
	if (!ctlcfg->sockapp[sockidx]) ctlcfg->sockapp[sockidx] = app;

	/* show prompt based on what app we are configuring */
	sprintf(response, "%s> ", ctlcfg->sockapp[sockidx]->name);
	send_network_reply(sock, response, strlen(response), client);
//	tcp_srv_respond_term(sock, response);

	return ret;
}

int
handle_battery_requests(void *arg, int sockidx, int len, void *dest)
{
	//A void pointer is used above to allow request_handler pointer in app config to have consistent format,
	//since sent_to_uart is also one of the handlers that gets used, but doesn't produce any network traffic,
	//and I wanted to keep network-related things out of there.
	struct freertos_sockaddr *client = (struct freertos_sockaddr *)dest;
	APP_CFG *app = (APP_CFG *)arg;
	Socket_t sock = app->clientsocks[sockidx];

	int ret;
	
	ret = parse_battery_cmd(app, sockidx, len, client);
	if (ret) return (ret);
	
	if (!is_socket_udp(sock)){
		//Only send back a prompt if using TCP comms
		send_network_reply_term(sock, "battery> ", client);
	}
	else{
		//UDP comms, should have been already handled above, don't bother with sending prompt
	}
//	send_network_reply(sock, response, strlen(response), client);
//	tcp_srv_respond_term(sock, response);

	return ret;
}

static void
commTask(void *parm) {

	TASK_CFG *task = (TASK_CFG *)parm;
	APP_CFG *app;
	struct freertos_sockaddr client;
	int i, j;
	BaseType_t res, sres;
	socklen_t len;
	Socket_t newsock;
	char response[64];
//	const TickType_t xReceiveTimeOut = TCP_RECV_TIMEOUT / portTICK_RATE_MS;
	TickType_t xSelectTimeout = 100 / portTICK_RATE_MS;
	
	len = sizeof(client);

	/*
	 * In a serial task we don't want to waste time waiting for
	 * TCP activity since we might drop data on the UART RX end.
	 */
	if (task->serial) xSelectTimeout = 0;

	task->active_fds = FreeRTOS_CreateSocketSet();
	
	/* create listener sockets for all apps in task */
	for (i = 0; i < task->appnum; i++) {
		app = task->apps[i];
		//TCP Sockets
		app->master_tcp_sock = tcp_sock_init(app->tcp_port);
		FreeRTOS_FD_SET(app->master_tcp_sock, task->active_fds, eSELECT_READ | eSELECT_EXCEPT);

		//UDP Sockets
		newsock = udp_sock_init(app->udp_port);
		//a globally accessable copy of the udp batt comms socket to facilitate broadcasting outside of the commTask.
		if (!strncmp(app->name,"battery", 7)){
			default_udp_sock = newsock;
		}

		for (j = 0; j < MAX_APP_CLIENTS; j++) {
			if (!app->clientsocks[j]) {
				FreeRTOS_FD_SET(newsock, task->active_fds, eSELECT_READ | eSELECT_EXCEPT);
				app->clientsocks[j] = newsock;
				break;
			}
		}
	}
	
	while (1) {
		/* the serial task checks for any bytes in the UART queue to send out */
#if defined(ENABLE_UART_BRIDGES)
		if (task->serial) {
			for (i = 0; i < task->appnum; i++) {
				recv_from_uart(task->apps[i]);
			}
		}
#endif

		/*
		 * TODO: The FreeRTOS version of select() returns the socket which
		 * triggered the event. We might be able to rewrite this to be more
		 * efficient. For now we keep the logic we used with the Interniche
		 * stack.
		 */
		sres = FreeRTOS_select(task->active_fds, xSelectTimeout);
		if (sres  < 0) {
			panic ("select");
		} else if (sres == 0) {
			// nothing happening on any socket
			continue;
		}
		
		/* cycle through configured apps and check for incoming requests */ 
		for (i = 0; i < task->appnum; i++) {
			app = task->apps[i];

			/* check for requests on established connections */
			for (j = 0; j < MAX_APP_CLIENTS; j++) {
				if (!app->clientsocks[j] || !FreeRTOS_FD_ISSET(app->clientsocks[j], task->active_fds)){
					continue;
				}

				res = recv_network_data(app->clientsocks[j],
										app->tcp_buffer,
										net_tcp_buffsize,
										0,
										&client,
										&len);

				/* data arriving on an already-connected socket. */
//				res = FreeRTOS_recv(app->clientsocks[j],
//									app->tcp_buffer,
//									net_tcp_buffsize,
//									0);

				//Handle the incoming request (points to handle_battery_requests(...), or handle_ctl_requests(...) depending on app config)
				if (res <= 0 || app->request_handler(app, j, res, &client) < 0) {
					/* Read error (-1) or EOF (0) */
					utils_socket_close(app->clientsocks[j]);
					FreeRTOS_FD_CLR(app->clientsocks[j], task->active_fds, eSELECT_READ | eSELECT_EXCEPT);
					app->clientsocks[j] = 0;
					continue;
				}

			if (app->app_type != SERIAL_APP) taskYIELD();
			}

			/* check for incoming new connections on master sock */
			if (!app->master_tcp_sock || !FreeRTOS_FD_ISSET(app->master_tcp_sock, task->active_fds))
				continue;

			newsock = FreeRTOS_accept(app->master_tcp_sock, &client, &len);
			if (newsock < 0) {
				perror ("accept");
				panic ("EXIT_FAILURE");
			}

//			FreeRTOS_setsockopt(newsock,
//								0,
//								FREERTOS_SO_RCVTIMEO,
//								&xReceiveTimeOut,
//								sizeof( xReceiveTimeOut ) );

			for (j = 0; j < MAX_APP_CLIENTS; j++) {
				if (!app->clientsocks[j]) {
					FreeRTOS_FD_SET(newsock, task->active_fds, eSELECT_READ | eSELECT_EXCEPT);
					app->clientsocks[j] = newsock;
					break;
				}
			}
			if (j >= MAX_APP_CLIENTS) {
				tcp_srv_respond_term(newsock,
				  "\r\nToo many active connections, closing!\r\n\n");
				utils_socket_close(newsock);
				continue;

			}
			if (xPortGetFreeHeapSize() < TCP_MIN_FREE_HEAP) {
				tcp_srv_respond_term(newsock,
				  "\r\nRunning low on memory, closing!\r\nConsider closing unused connections.\r\n\n");
				utils_socket_close(newsock);
				continue;
			}
			if (app->app_type != SERIAL_APP) {
				sprintf(response, "\xff\xfd\x22\xff\xfa\x22\x01\x01\xff\xf0\r\n\nConnected to %s \r\n%s> ",
				  app->name, app->name);
				tcp_srv_respond_term(newsock, response);
			}
			if (app->app_type != SERIAL_APP) taskYIELD();
		}
		if (!task->serial) utils_task_sleep(50);
	}
}

static void
batteryTask(void *parm) {
	init_batteries();
//	init_sensors();		// Goes in the batteryTask to avoid SMBus interference as they both use the same I2C hardware
	if (xSemaphoreTake(bat_semphr, 500 / portTICK_RATE_MS) == pdTRUE) {
		init_EEPROM();		// Goes in the batteryTask to avoid SMBus interference as they both use the same I2C hardware
		xSemaphoreGive(bat_semphr);
	}


	for (;;) {
		/* check state of CHG HS switch to determine LED status */
//		if (Chip_GPIO_GetPinState(LPC_GPIO, 0, CHG_ENABLE_PIN))
//			set_led_charge_status();
//		else
//			set_led_discharge_status();

		if (xSemaphoreTake(bat_semphr, 500 / portTICK_RATE_MS) == pdFALSE) {
			continue;
		}
		update_battery_status();
		xSemaphoreGive(bat_semphr);

		if (xSemaphoreTake(comms_semphr, 500 / portTICK_RATE_MS) == pdFALSE) {
			continue;
		}
		//protect the secondary buffers when copying fresh data into them, as they can
		//be read asynchronously by UI request
		update_batt_bin_struct();
		xSemaphoreGive(comms_semphr);

		/* sleep for x ms before next time through the loop */
		vTaskDelay(BATT_UPDATE_DELAY_S * 1000 / portTICK_RATE_MS);
	}
}	

int
load_flash_config(void)
{
	void *fptr = (void *) FLASH_CFG_LOCATION;
	int i;
	uint16_t sum = 1;

	if (FLASH_BYPASS) return (1);

	/* sync in-ram copy */
	memcpy(&flash_cfg, fptr, FLASH_CFG_SIZE);
	
	/* verify data is valid */
	for (i = 0; i < sizeof(FLASH_CFG); i++) {
		sum = (sum + ((uint8_t *)&flash_cfg)[i]) % 256;
	}
	/* chksum field is not part of the checksum */
	sum = (sum - flash_cfg.chksum) % 256;
	sum &= 0xff;
	
	if (sum != flash_cfg.chksum) {
		/* flash location is not a valid config */
		memset(&flash_cfg, 0, FLASH_CFG_SIZE);
		return (1);
	}
	return (0);
}

void
store_flash_config(void)
{
	int i;
	uint16_t sum = 1;
	void *fptr = (void *) FLASH_CFG_LOCATION;
	
	if (FLASH_BYPASS) return;

	flash_cfg.chksum = 0;
	/* calculate checksum before storing data */
	for (i = 0; i < sizeof(FLASH_CFG); i++) {
		sum = (sum + ((uint8_t *)&flash_cfg)[i]) % 256;
	}
	flash_cfg.chksum = sum;
		
	(void) iap_prepare_sector(29, 29);
	(void) iap_erase_sector(29, 29);
	(void) iap_prepare_sector(29, 29);
	(void) iap_copy_ram_to_flash(&flash_cfg, fptr, FLASH_CFG_SIZE);
}

static void
main_task_setup(TASK_CFG *maintask)
{
	int id;
	bool flash_valid = false;
	char *srv_inbuf;

	/* Enable MCU_BAT_EN */
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, 16); /* GPIO - MCU_BAT_EN */
	Chip_GPIO_SetPinOutHigh(LPC_GPIO, 0, 16);	/* NMOSFET so pull high to enable */

#if defined(ENABLE_RTC_TIME)
	init_RTC();
#endif

	/* alloc TCP recv buffers for main and serial tasks */
	if ((srv_inbuf = (char *)pvPortMalloc(net_tcp_buffsize)) == NULL) panic(NULL);
	memset(srv_inbuf, 0, net_tcp_buffsize);
	
	/* zero task and app configuration structs */
	memset(maintask, 0, sizeof(TASK_CFG));
	memset(&control_app, 0, sizeof(control_app));
	memset(&control_cfg, 0, sizeof(control_cfg));
	memset(&battery_app, 0, sizeof(battery_app));

	/* load network config from flash */
	if (load_flash_config() == 0) {
		memcpy(ucMACAddress, flash_cfg.macaddr, sizeof(ucMACAddress));
		strcpy(tcp_srv_ip, flash_cfg.ipstr);
		strcpy(tcp_srv_gw, flash_cfg.gwstr);
		strcpy(tcp_srv_nm, flash_cfg.nmstr);
		flash_valid = true;
	} else {
		/* there are no valid values in flash, store defaults for next time */
		memcpy(flash_cfg.macaddr, ucMACAddress, sizeof(ucMACAddress));
		strcpy(flash_cfg.ipstr, tcp_srv_ip);
		strcpy(flash_cfg.gwstr, tcp_srv_gw);
		strcpy(flash_cfg.nmstr, tcp_srv_nm);
	}

	/* setup for control app */
	id = 0;
	control_app.id = id;
	strcpy(control_app.name, "control");
	control_app.app_type = CTL_APP;
	control_app.request_handler = &handle_ctl_requests;
	control_app.tcp_buffer = srv_inbuf;
	control_app.parent_task = maintask;
	control_app.ext_app_cfg = &control_cfg;
	if (flash_valid){
		if ( (flash_cfg.app_cfgs[id].tcp_port > 0) && (flash_cfg.app_cfgs[id].udp_port > 0) ) {
			control_app.tcp_port = flash_cfg.app_cfgs[id].tcp_port;
			control_app.udp_port = flash_cfg.app_cfgs[id].udp_port;
		}
		else{
			//Should never have a port num < 0, if we do, assume all has gone to pots,
			//and use the defaults.
			flash_valid = false;
			control_app.tcp_port = tcp_base_port + id;
			control_app.udp_port = udp_base_port + id;
			flash_cfg.app_cfgs[id].tcp_port = tcp_base_port + id;
			flash_cfg.app_cfgs[id].udp_port = udp_base_port + id;
		}
	}
	 else {
		flash_valid = false;
		control_app.tcp_port = tcp_base_port + id;
		control_app.udp_port = udp_base_port + id;
		flash_cfg.app_cfgs[id].tcp_port = tcp_base_port + id;
		flash_cfg.app_cfgs[id].udp_port = udp_base_port + id;
	}
	flash_cfg.app_cfgs[id].flags = FLASH_CFG_TYPE_MASK(control_app.app_type);

#if defined(ENABLE_UART_BRIDGES)
	init_uart_ports(id, &control_cfg, flash_valid, &flash_cfg);
	control_cfg.apps[2] = &control_app;
	control_cfg.apps[3] = &battery_app;
	control_cfg.appnum = 4;
#else
	control_cfg.apps[0] = &control_app;
	control_cfg.apps[1] = &battery_app;
	control_cfg.appnum = 2;
#endif
	
	/* setup for battery app */
	id++;
	battery_app.id = id;
	strcpy(battery_app.name, "battery");
	battery_app.app_type = BAT_APP;
	battery_app.request_handler = &handle_battery_requests;
	battery_app.tcp_buffer = srv_inbuf;
	battery_app.parent_task = maintask;
	if (flash_valid){
		if ((flash_cfg.app_cfgs[id].tcp_port > 0) && (flash_cfg.app_cfgs[id].udp_port > 0)) {
			battery_app.tcp_port = flash_cfg.app_cfgs[id].tcp_port;
			battery_app.udp_port = flash_cfg.app_cfgs[id].udp_port;
		}
		else{
			flash_valid = false;
			battery_app.tcp_port = tcp_base_port + id;
			battery_app.udp_port = udp_base_port + id;
			flash_cfg.app_cfgs[id].tcp_port = tcp_base_port + id;
			flash_cfg.app_cfgs[id].udp_port = udp_base_port + id;
		}
	}
	else {
		flash_valid = false;
		battery_app.tcp_port = tcp_base_port + id;
		battery_app.udp_port = udp_base_port + id;
		flash_cfg.app_cfgs[id].tcp_port = tcp_base_port + id;
		flash_cfg.app_cfgs[id].udp_port = udp_base_port + id;
	}
	flash_cfg.app_cfgs[id].flags = FLASH_CFG_TYPE_MASK(battery_app.app_type);

	/* if we had to use default values for anything, store new values */
	if (!flash_valid) {
		store_flash_config();
	}
	
	/* assign apps to tasks */
	maintask->appnum = 2;
	maintask->apps[0] = &control_app;
	maintask->apps[1] = &battery_app;

	return;
}

/* Sets up system hardware */
static void prvSetupHardware(void)
{
	SystemCoreClockUpdate();
	Board_Init();
	smbus_setup();
	Indicators_Init();
}

int
main(void)
{
	/*
	 * Set up the heap for FreeRTOS.
	 * We are using all of the AHB RAM but there is still some space
	 * left in the local RAM so we make some use of that too by creating
	 * an uint8_t array and pass this along as additional heap space.
	 * See: https://docs.aws.amazon.com/freertos-kernel/latest/dg/heap-management.html#vportdefineheapregions-api-function
	 */
#define MAIN_ADDITIONAL_HEAP_SIZE	0x3000

	static uint8_t ucHeap[MAIN_ADDITIONAL_HEAP_SIZE];
	const HeapRegion_t xHeapRegions[] =
	{
		{ ucHeap, MAIN_ADDITIONAL_HEAP_SIZE },
		{ ( uint8_t * ) 0x2007C000UL, 0x8000 },
		{ NULL, 0 } /* Terminates the array. */
	};
	vPortDefineHeapRegions( xHeapRegions );
	prvSetupHardware();
	main_task_setup(&maintask);
	
	vSemaphoreCreateBinary(bat_semphr);
	vSemaphoreCreateBinary(comms_semphr);

	/*
	 * FreeRTOS_IPInit wants the IP addr, etc in a weird number array
	 * format so we just convert the internally used uint32_t value
	 * and cast it to an uint8_t array.
	 * Note: this might not work on big-endian platforms
	 */
	uint32_t sip = (uint32_t) FreeRTOS_inet_addr(tcp_srv_ip);
	uint32_t snm = (uint32_t) FreeRTOS_inet_addr(tcp_srv_nm);
	uint32_t sgw = (uint32_t) FreeRTOS_inet_addr(tcp_srv_gw);
	uint32_t dns = 0UL;

	FreeRTOS_IPInit(
	  (uint8_t *)&sip,	// server IP address
	  (uint8_t *)&snm,	// netmask
	  (uint8_t *)&sgw,	// gateway
	  (uint8_t *)&dns,	// DNS server
	  ucMACAddress);	// MAC address

	/* start battery query task */
	xTaskCreate(batteryTask, "battery",
	  configMINIMAL_STACK_SIZE, NULL, 2, &to_ts_battery);

	/*
	 * The comm tasks are started after the network is up
	 * in vApplicationIPNetworkEventHook() below.
	 */

	/* enable watchdog */
	if (WATCHDOG_ON) init_wdt(WATCHDOG_TIMEOUT);

	vTaskStartScheduler();

	for( ;; ) {
	};

}



/*----------------- FreeRTOS Application Hooks ----------------------------*/

/* FUNCTION: vApplicationTickHook()
 *
 * Called by the FreeRTOS timer interrupt function.
 */
void
vApplicationTickHook(void)
{
}


#if (configUSE_IDLE_HOOK == 1)

/* FUNCTION: vApplicationIdleHook()
 *
 * Called from the "idle" task.
 *
 * PARAMS: none
 *
 * RETURN: none
 */
void
vApplicationIdleHook(void)
{
   const unsigned long ulMSToSleep = 5;

   /* Sleep to reduce CPU load, but don't sleep indefinitely in case
    * there are tasks waiting to be terminated by the idle task.
    */
//   Sleep(ulMSToSleep);
   Chip_PMU_SleepState(LPC_PMU);
}

#endif

#if (configUSE_MALLOC_FAILED_HOOK == 1)

/* FUNCITON: vApplicationMallocFailedHook()
 *
 * Called by FreeRTOS when a memory allocation request fails.
 *
 * PARAMS: none
 *
 * RETURN: none
 */
void
vApplicationMallocFailedHook(void)
{
   //panic("malloc");
	for(;;) {
	}
}

#endif

#if (configCHECK_FOR_STACK_OVERFLOW > 0)

/* FUNCTION: vApplicationStackOverflowHook()
 *
 * Called if the OS detects a task stack overflow.
 *
 * PARAM1: xTaskHandle        current FreeRTOS task
 * PARAM2: char *             name of current task
 *
 * RETURN: none
 */
void
vApplicationStackOverflowHook(xTaskHandle curTCB, char *name)
{
	//panic("stack overflow");
	for(;;) {
	}
}
#endif


/* Called by FreeRTOS+UDP when the network connects. */
void vApplicationIPNetworkEventHook( eIPCallbackEvent_t eNetworkEvent )
{
static BaseType_t xTaskAlreadyCreated = pdFALSE;

	if( eNetworkEvent == eNetworkUp )
	{
		/* Create the tasks that transmit to and receive from a standard
		echo server (see the web documentation for this port) in both
		standard and zero copy mode. */
		if( xTaskAlreadyCreated == pdFALSE )
		{
			/* Battery and control communication task
			 * Note: stack depth is in words, not bytes.
			 * commTask stack depth doubled following UDP integration,
			 * as stackoverflow was occurring.
			 */
			xTaskCreate(commTask, "comm",
			  configMINIMAL_STACK_SIZE*2, &maintask, 2, &to_ts_comm);

#if defined(ENABLE_UART_BRIDGES)  /* Serial comm task */
			xTaskCreate(commTask, "sercomm",
			  configMINIMAL_STACK_SIZE, &serialtask, 2, &to_ts_sercomm);
#endif
		}
	}
}
/*-----------------------------------------------------------*/


#if (ipconfigSUPPORT_OUTGOING_PINGS != 0)
/* Called by FreeRTOS+TCP when a reply is received to an outgoing ping request. */
void vApplicationPingReplyHook( ePingReplyStatus_t eStatus, uint16_t usIdentifier )
{
}
#endif


/* FUNCTION: vAssertCalled()
 *
 * Called if "configAssert(x)" is defined in FreeRTOSConfig.h.
 *
 * PARAMS: none
 *
 * RETURN: none
 */
void
vAssertCalled(void)
{
	for(;;) {
	}
}

void
panic(char *msg) {
	for(;;) {
	}
}

/*
 * ExternalEEPROM.cpp
 *
 *  Created on: Mar 20, 2021
 *      Author: Jedediah Smith (j.smith@loonatec.com)
 */

#include "external_eeprom.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "smbus.h"
#include "utils.h"
#include "chip.h"

#include "indicators.h"
#include "rtc_time.h"

#define SET_BIT(value, pos) (value |= (1U<< pos))
#define CLEAR_BIT(value, pos) (value &= (~(1U<< pos)))
#define IS_BIT_SET(value, pos) (value & (1U<< pos))

// --- Global variables from main source ---
extern char str_data[];
extern xSemaphoreHandle bat_semphr;
extern xSemaphoreHandle comms_semphr;

// Memory Settings
#define EEPROM_MEM_ADDR 	0b1010000		// Device address for memory array
#define EEPROM_MEM_SIZE   	65536			// Size in Bytes
#define EEPROM_PAGE_SIZE  	128				// Bytes per memory page, 512 pages
#define EEPROM_WRITE_TIME 	5				// Time in ms to write page
#define EEPROM_LOG_SIZE		8				// Size in bytes of a log entry

// --- Static functions ---
static bool testConnection(void);
static void read_page(uint16_t);
static void write_page();
static void increment_log_pos(void);
static void zero_eeprom_buffer(void);
static void change_eeprom_memory_location(int);

static uint8_t eeprom_buffer[EEPROM_PAGE_SIZE + 2];	// Note: first two bytes are memory location for next entry
static bool eeprom_enabled = false;

static uint8_t battery_tracker[55] = {0};	// Battery state flags to avoid repeated event logging
static uint8_t sensor_tracker[3] = {0};		// Sensor tracker to avoid repeated event logging

/**
 * \brief Test if EEPROM is present, if so then find next log location
 */
void init_EEPROM(void) {
	if ( testConnection() == false ) { return; }
		eeprom_enabled = true;

		/* Start going through each memory page looking for a zero'd entry */
		for (int page = 0; page < EEPROM_MEM_SIZE; page += EEPROM_PAGE_SIZE) {
			read_page(page);

			/* Look at each entry on that page for a zero'd one */
			for (int entry = 0; entry < EEPROM_PAGE_SIZE; entry += EEPROM_LOG_SIZE) {
				if (eeprom_buffer[entry+2] == empty_record ) {
					change_eeprom_memory_location(page+entry);
					return;	// -- We have found a winner!
				}
			}
		}
}


/**
 * \brief Display the state of the EEPROM to the user
 */
int display_state_EEPROM(char *strPtr) {
	if (eeprom_enabled) {
		int current_record = ((eeprom_buffer[0] << 8) | eeprom_buffer[1]) / EEPROM_LOG_SIZE;
		return sprintf(strPtr,"   EEPROM: Record %d of %d\r\n", current_record, EEPROM_MEM_SIZE/EEPROM_LOG_SIZE);
			// Note: displays current record index, doesn't account for when log wraps. Would need to look at next record to see if it is zero
	} else {
		return sprintf(strPtr,"   EEPROM: Disabled\r\n");
	}
}

/**
 * \brief Erase entire EEPROM and write 0's
 */
void erase_EEPROM(Socket_t sock, struct freertos_sockaddr *dst_address) {
	if (eeprom_enabled == false) { return; }
	char *strPtr;

	zero_eeprom_buffer();

	if (xSemaphoreTake(comms_semphr, 1000 / portTICK_RATE_MS) == pdFALSE) {
		send_network_reply_term(sock, " - ERROR: Erasing EEPROM Failed! (1)\r\n\r\n", dst_address);
		return;
	}

	if (xSemaphoreTake(bat_semphr, 1000 / portTICK_RATE_MS) == pdFALSE) {
		send_network_reply_term(sock, " - ERROR: Erasing EEPROM Failed! (2)\r\n\r\n", dst_address);
		return;
	}

	send_network_reply_term(sock, " - Erasing EEPROM address:\r\n", dst_address);

	for (int addr = 0; addr < EEPROM_MEM_SIZE; addr += EEPROM_PAGE_SIZE) {
		change_eeprom_memory_location(addr);
		write_page();

		strPtr = str_data;
		strPtr += sprintf(strPtr, "   %.5d", addr);
		if ( addr % 8 == 0 ) {
			strPtr += sprintf(strPtr, "\r\n");
			send_network_reply(sock, str_data, strPtr-str_data, dst_address);
		}
	}

	send_network_reply_term(sock, " - Done Erasing EEPROM \r\n\r\n", dst_address);
	xSemaphoreGive(bat_semphr);
	xSemaphoreGive(comms_semphr);
}

/**
 * \brief Records an event to EEPROM memory
 */
void log_entry(log_event_types event, log_device_types device, uint16_t data) {
	if ( testConnection() == false ) {
		que_led_error();
		return;
	}

	int page_pos = (eeprom_buffer[0] << 8) | (eeprom_buffer[1] & 0x80);
	int entry_pos = eeprom_buffer[1] & 0x7F;
	uint32_t currentTime = encodeTime();

	/* Make sure eeprom_buffer is populated with current page */
	read_page(page_pos);

	/* Add new entry to eeprom_buffer */
	eeprom_buffer[2 + entry_pos] = event;				// Save event type
	eeprom_buffer[3 + entry_pos] = device;				// Save device type

	eeprom_buffer[4 + entry_pos] = data >> 8;			// Save data
	eeprom_buffer[5 + entry_pos] = data & 0xFF;

	eeprom_buffer[6 + entry_pos] = (uint8_t)(currentTime >> 24);	// Save timestamp
	eeprom_buffer[7 + entry_pos] = (uint8_t)(currentTime >> 16);
	eeprom_buffer[8 + entry_pos] = (uint8_t)(currentTime >> 8);
	eeprom_buffer[9 + entry_pos] = (uint8_t)(currentTime & 0xFF);

	if ( (entry_pos + EEPROM_LOG_SIZE) > EEPROM_PAGE_SIZE) {
		change_eeprom_memory_location(page_pos);
		write_page();
		change_eeprom_memory_location(page_pos+entry_pos);
		increment_log_pos();	// Go to next page or wrap around memory
		for (int i = 2; i < EEPROM_LOG_SIZE; i ++) { eeprom_buffer[i] = 0; }
		write_page();
	} else {
		for (int i = entry_pos + 10; i < EEPROM_LOG_SIZE; i ++) { eeprom_buffer[i] = 0; }
		change_eeprom_memory_location(page_pos);
		write_page();
		change_eeprom_memory_location(page_pos+entry_pos);
		increment_log_pos();
	}
}

/**
 * \brief Sends each EEPROM page via telnet packet to user
 */
void display_log(Socket_t sock, struct freertos_sockaddr *dst_address) {
	if (eeprom_enabled == false) {
		send_network_reply_term(sock, " - ERROR: EEPROM not enabled\r\n", dst_address);
		return;
	}

	uint16_t startPage = (eeprom_buffer[0] << 8) | (eeprom_buffer[1] & (0x80));
	uint16_t startEntry = eeprom_buffer[1] & (EEPROM_PAGE_SIZE-1);
	int tempPage = startPage;
	int tempEntry = EEPROM_PAGE_SIZE - EEPROM_LOG_SIZE; //startEntry;

	char *strPtr;
	log_event_types event;
	log_device_types device;
	uint16_t data;
	uint32_t timestamp;
	RTC_TIME_T timestamp_d;

	if (xSemaphoreTake(comms_semphr, 30000 / portTICK_RATE_MS) == pdFALSE) {
		send_network_reply_term(sock, "   Pack access failed!\r\n", dst_address);
		return;
	}

	do {
		read_page(tempPage);
		strPtr = str_data;

		for (int i = tempEntry; i > -1; i -= EEPROM_LOG_SIZE) {
			/* eeprom_buffer bit field check
			 * 	If EEPROM_LOG_SIZE = 8 and EEPROM_PAGE_SIZE = 24
			 * 	0 & 1 are eeprom memory locations
			 * 	2-9   are entry #1
			 * 	10-17 are entry #2
			 * 	18-25 are entry #3
			 */
			event = eeprom_buffer[2 + i];
			device = eeprom_buffer[3 + i];

			data = eeprom_buffer[4 + i] << 8;
			data |= eeprom_buffer[5 + i];

			timestamp = eeprom_buffer[6 + i] << 24;
			timestamp |= eeprom_buffer[7 + i] << 16;
			timestamp |= eeprom_buffer[8 + i] << 8;
			timestamp |= eeprom_buffer[9 + i];

			timestamp_d = decodeTime(timestamp);

			if (event) {
				strPtr += sprintf(strPtr, "   Entry %.3d:%.3d - ", i, tempPage);
				strPtr += sprintf(strPtr, " %.2d:%.2d:%.2d %.2d/%.2d/%.4d -",
						timestamp_d.time[RTC_TIMETYPE_HOUR],
						timestamp_d.time[RTC_TIMETYPE_MINUTE],
						timestamp_d.time[RTC_TIMETYPE_SECOND],
						timestamp_d.time[RTC_TIMETYPE_DAYOFMONTH],
						timestamp_d.time[RTC_TIMETYPE_MONTH],
						timestamp_d.time[RTC_TIMETYPE_YEAR]);
				strPtr += sprintf(strPtr, " %d, %d, %d - \r\n", event, device, data);
			}

		}

		/* Adjust entry and page pointers */
		tempEntry = EEPROM_PAGE_SIZE - EEPROM_LOG_SIZE;
		tempPage -= EEPROM_PAGE_SIZE;
		if ( tempPage < 0 ) { tempPage = EEPROM_MEM_SIZE - EEPROM_PAGE_SIZE; }

		/* Send data from this page to the user, if we have any */
		if (strPtr != str_data) {
			send_network_reply(sock, str_data, strPtr-str_data, dst_address);
			vTaskDelay(10 / portTICK_RATE_MS);
		}
	} while ( tempPage != startPage );

	send_network_reply_term(sock, "--- End Log ---\r\n\n", dst_address);
	xSemaphoreGive(comms_semphr);
	change_eeprom_memory_location(startPage+startEntry);
}

void log_battery_event(log_event_types event, int battery) {
	if ( testConnection() == false ) { return; }

	if ( IS_BIT_SET(battery_tracker[battery], event) ) {
		// This event on this battery has already been logged
	} else {
		SET_BIT(battery_tracker[battery], event);
		log_entry(event, battery, 0);
		vTaskDelay(20 / portTICK_RATE_MS);
	}
}

void log_sensor_event(log_event_types event, log_device_types sensor) {
	if ( testConnection() == false ) { return; }

	if ( IS_BIT_SET(sensor_tracker[(sensor-192)], (event-32)) ) {
		// This event on this sensor has already been logged
	} else {
		SET_BIT(sensor_tracker[(sensor-192)], (event-32));
		log_entry(event, sensor, 0);
		vTaskDelay(20 / portTICK_RATE_MS);
	}
}

// ----- Private Functions ---- //

/**
 * \brief Reads a page to memory
 */
void read_page(uint16_t page) {
	if (eeprom_enabled == false) { return; }

	zero_eeprom_buffer();
	change_eeprom_memory_location(page);

	if (testConnection() ) {	// Ensure EEPROM is available, waits for previous request
		I2C_XFER_T xfer = {0};
		xfer.slaveAddr = EEPROM_MEM_ADDR;
		xfer.txBuff = eeprom_buffer;
		xfer.txSz = 2;
		xfer.rxBuff = eeprom_buffer + 2;
		xfer.rxSz = EEPROM_PAGE_SIZE;
		while (Chip_I2C_MasterTransfer(I2C0, &xfer) == I2C_STATUS_ARBLOST) {}
		if ( xfer.rxSz != 0 ) {
			que_led_error(); 	// Read error
		}
	}
}

/**
 * \brief Writes a page to memory
 */
void write_page() {
	if ( eeprom_enabled == false ) { return; }

	if (testConnection() ) {	// Ensure EEPROM is available, waits for previous request
		if (Chip_I2C_MasterSend(I2C0, EEPROM_MEM_ADDR, eeprom_buffer, (EEPROM_PAGE_SIZE+2)) != (EEPROM_PAGE_SIZE+2)) {
			que_led_error();	// Write error
		}
	} else {
		que_led_error(); 		// Write error
	}
}

/**
 * \brief Look for ACK from device
 *
 * \return bool :
 * 			- True: Device is detected/online
 * 			- False: Device does not ACK within max EEPROM write period
 */
bool testConnection(void) {
	if (!setSmbusMux(5, 7)) {
		inc_totalSMBusErrors();
		que_led_error();	// Write error
		return false;
	}

	int8_t max_delay_ms = EEPROM_WRITE_TIME + 3;

	do {
		if ( Chip_I2C_MasterSend(I2C0, EEPROM_MEM_ADDR, NULL, 1) == 1) { return true; }
		utils_block_sleep(1);
		max_delay_ms--;
	} while (max_delay_ms > 0);

	return false;
}

/**
 * \brief Increments the log location, EEPROM module handles wrapping
 */
void increment_log_pos() {
	int next_pos = (eeprom_buffer[0] << 8) | eeprom_buffer[1];
	change_eeprom_memory_location(next_pos + EEPROM_LOG_SIZE);
}

/**
 * \brief Ensure buffer is filled with all zeros
 */
void zero_eeprom_buffer(void) {
	for (int x = 0; x < EEPROM_PAGE_SIZE; x++) {
		eeprom_buffer[x] = 0x00;
	}
}

/**
 * \brief Change the EEPROM memory location in first two bytes of eeprom_buffer
 */
void change_eeprom_memory_location(int new_pos) {
	eeprom_buffer[0] = (uint8_t)(new_pos >> 8);		// MSB
	eeprom_buffer[1] = (uint8_t)(new_pos & 0xFF);	// LSB
}


/*
 * externalEEPROM.h
 *
 *  Created on: Mar 20, 2021
 *      Author: Jedediah Smith (j.smith@loonatec.com)
 *
 *  Warning: this is not designed to handle larger memory modules that have upper and lower
 *  	sections that are accessed by different addresses
 *
 *  Designed to use: https://www.onsemi.com/pdf/datasheet/cat24c512-d.pdf
 *  	512 Kb EEPROM memory
 *  	8-bit word
 *
 *  Uses a 64-bit page per log entry (~8k entries on 512 Kb)
 *  	0-31:	RTC time
 *  	32-39: 	log_event_types
 *  	40-47: 	log_device_types
 *  	48-63:	log data
 *
 *  Two pages are written per log entry
 *  	1: the page with the current log record
 *  	2: the next page is zero'd to indicate start point
 */

#ifndef EXTERNAL_EEPROM_H_
#define EXTERNAL_EEPROM_H_

#include "board.h"
#include "net.h"

#define LOG_BATTERY_HALF_CAPACITY 1500	// mAh level at which to log "pack_50_percent"

typedef enum  {
	empty_record,						// Used to identify the next log location

		/* Note: the following group is also used for battery flag bit flags */
	pack_over_temp_alarm,				// Pack alarm code 0x1000
	pack_term_charge_alarm,				// Pack alarm code 0x4000
	pack_term_dschg_alarm,				// Pack alarm code 0x0800
	pack_remaining_capacity_alarm,		// Pack alarm code 0x0200	Note: default is 10%
	pack_50_percent,					// Pack capacity reached 50%

	pack_over_charged_alarm,			// Pack alarm code 0x8000
	pack_remaining_time_alarm,			// Pack alarm code 0x0100	Note: default is 10 min

	battery_discharge_enable,			// Telnet commands in srv_ui.c
	battery_charge_enable,
	battery_all_off,
	battery_shutdown,
	battery_aux_enable,
	battery_aux_disable,
	battery_aux2_enable,
	battery_aux2_disable,

	sensor_temperature_high = 32,		// Temperature sensor above threshold
	sensor_pressure_high = 33,			// Pressure sensor above threshold
	sensor_humidity_high = 34,			// Humidity sensor above threshold

	smb_error,							// An error occurred on the SMBus
	last_event_type
} log_event_types;

typedef enum {
	// 0-54 are reserved for battery pack id's
	// 64-127 are reserved for SMBus Mux id's

	telnet_command = 128,
	battery_command,

	temperature_sensor = 192,			// Note: these numbers are used to reference sensor_tracker[]
	pressure_sensor = 193,
	humidity_sensor = 194,
	last_device_type
} log_device_types;

/**
 * \brief Check whether EEPROM is connected, disables if not
 */
void init_EEPROM(void);


/**
 * \brief Display current state of EEPROM
 *
 * \return int : number of bytes written to string
 */
int display_state_EEPROM(char *strPtr);

/**
 * \brief Display current data in EEPROM memory
 */
void display_log(Socket_t sock, struct freertos_sockaddr *dst_address);

/**
 * \brief Logs an event to EEPROM memory. Uses RTC time to timestamp event
 */
void log_entry(log_event_types event, log_device_types device, uint16_t data);

/**
 * \brief Erase the entire memory
 */
void erase_EEPROM(Socket_t sock, struct freertos_sockaddr *dst_address);

/**
 * \brief Write data to memory
 */
void read_EEPROM(uint32_t eepromLocation, uint8_t *buff, uint16_t bufferSize);

/**
 * \brief Read data from memory
 */
void write_EEPROM(uint32_t eepromLocation, const uint8_t *dataToWrite, uint16_t blockSize);

/**
 * \brief Log a battery event to memory. Only does each flag once per battery.
 */
void log_battery_event(log_event_types flag, int battery);

/**
 * \brief Log a sensor event to memory. Only does each flag once per sensor.
 */
void log_sensor_event(log_event_types event, log_device_types sensor);

#endif /* EXTERNAL_EEPROM_H_ */

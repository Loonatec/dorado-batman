/****************************************************************************/
/* Copyright (c) 2021 Balloon Ascent Technologies                           */
/****************************************************************************/
/* Summary  :                                                               */
/* Filename : indicators.c                                                  */
/* Author   : Jedediah Smith (j.smith@loonatec.com)                         */
/* Project  :                                                               */
/* Version  : 1.0                                                           */
/* Created  : March 2021                                                    */
/* Modified :                                                               */
/* Archived :                                                               */
/****************************************************************************/
/* Modification History:                                                    */
/****************************************************************************/

#include "indicators.h"
#include "smbus.h"

static bool once_error_flag = false;
static bool always_error_flag = false;
static int error_led_color, status_led_color, hb_led_color;
static int rit_count = 0;

void Indicators_Init(void) {
	Chip_RIT_Init(LPC_RITIMER);
	Chip_RIT_SetTimerInterval(LPC_RITIMER, RIT_INTERVAL);
	NVIC_EnableIRQ(RITIMER_IRQn);
}

void que_led_error(void) {
	once_error_flag = true;
}

void set_led_error(void) {
	always_error_flag = true;
}

void RIT_IRQHandler(void) {
	/* Clear interrupt */
	Chip_RIT_ClearInt(LPC_RITIMER);

	error_led_color = 0;
	status_led_color = 0;
	hb_led_color = 0;

	/* Handle counting */
	if (++rit_count > LED_CYCLE * RIT_SECOND) { rit_count = 0; }

	/* STATUS LED control */
	if (Chip_GPIO_GetPinState(LPC_GPIO, 0, CHG_ENABLE_PIN)) {				// Charge State
		status_led_color = 10 * rit_count;
	} else {
		if (rit_count <= RIT_SECOND) {
			if (Chip_GPIO_GetPinState(LPC_GPIO, 0, DISCH_ENABLE_PIN)) {		// Discharge State
				if ( rit_count % LED_INTERVAL == 0) {
	//				discharge_count = cycle_count / LED_INTERVAL;			// TODO: change flash number depending on capacity remaining
					status_led_color = 1000;
				}
			} else {														// Not charging or discharging, just on
				// Not charging or discharging state here
			}
		}
	}

	/* Heartbeat LED control */
	if (rit_count % RIT_SECOND == 0) {
		hb_led_color = 1000;
		Chip_WWDT_Feed(LPC_WWDT);
	}

	/* ERROR LED control  */
	if (once_error_flag || always_error_flag) {
		if ( rit_count % LED_INTERVAL == 0 ) {
			error_led_color = 1000;
			once_error_flag = false;
		}
	}

	/* Set PWM values */
	Board_LED_Color(error_led_color, status_led_color, hb_led_color);
}

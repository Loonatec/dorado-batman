/****************************************************************************/
/* Copyright (c) 2021 Balloon Ascent Technologies                           */
/****************************************************************************/
/* Summary  :                                                               */
/* Filename : indicators.h                                                  */
/* Author   : Jedediah Smith (j.smith@loonatec.com)                         */
/* Project  :                                                               */
/* Version  : 1.0                                                           */
/* Created  : March 2021                                                    */
/* Modified :                                                               */
/* Archived :                                                               */
/****************************************************************************/
/* Modification History:                                                    */
/****************************************************************************/

#ifndef INDICATORS_H_
#define INDICATORS_H_

#include "board.h"

#define RIT_INTERVAL	50			// Time in ms for Repetitive Interrupt Timer
#define RIT_SECOND		20			// Number of RIT events per second (1000/RIT_INTERVAL)
#define LED_INTERVAL	5			// Flash once every x RIT_INTERVALs
										// If RIT_INTERVAL=50 and LED_INTERVAL=5, then flash at 4 Hz
#define LED_CYCLE		5			// Number of seconds for entire LED cycle
										// Note: has to be ≥2 -- uses 1 second for each status and error LEDs

/**
 * \brief Start the Repetitive Interrupt Timer (RIT) for LEDs
 */
void Indicators_Init(void);

/**
 * \brief Set one-time Error LED flag
 *
 * \result A single, one-time flash
 */
void que_led_error(void);

/**
 * \brief Set permanent Error LED flag
 *
 * \result Ongoing double flash
 */
void set_led_error(void);

#endif /* INDICATORS_H_ */

/****************************************************************************/
/* Copyright (c) 2020 MBARI                                                 */
/* MBARI Proprietary Information. All rights reserved.                      */
/****************************************************************************/
/* Summary  :                                                               */
/* Filename : net.c                                                         */
/* Author   :                                                               */
/* Project  :                                                               */
/* Version  : 1.0                                                           */
/* Created  : 2020                                                          */
/* Modified :                                                               */
/* Archived :                                                               */
/****************************************************************************/
/* Modification History:                                                    */
/****************************************************************************/

#include "net.h"
#include "FreeRTOS_IP_Private.h"
#include "app_cfg.h"
#include "task.h"
#include "semphr.h"
#include "app_cfg.h"
#include "udp_types.h"

/* Timeout for recv(): 100ms */
#define TCP_RECV_TIMEOUT	(100)
/* Timeout for send(); prevents socket lockup bug when client socket is killed (improperly,
 * e.g. without calling close() while the micro is in FreeRTOS_send().
 */
#define TCP_SEND_TIMEOUT	(5000)

/* External global variables needed from batman.c */
extern FLASH_CFG flash_cfg;
extern APP_CFG battery_app;

//This socket is setup in batman.c and left here as a globally accessible battery socket should other
//units need/require it.
Socket_t default_udp_sock;

/* setup tcp socket for server */
Socket_t
tcp_sock_init(uint16_t port)
{
	struct freertos_sockaddr me;
	const TickType_t xReceiveTimeOut = TCP_RECV_TIMEOUT / portTICK_RATE_MS;
	const TickType_t xSendTimeOut = TCP_SEND_TIMEOUT / portTICK_RATE_MS;
	Socket_t elisten_sock;

	/* open TCP socket */
	me.sin_family = FREERTOS_AF_INET;
	me.sin_port = FreeRTOS_htons(port);

	if (((elisten_sock = FreeRTOS_socket(FREERTOS_AF_INET,
										FREERTOS_SOCK_STREAM,
										FREERTOS_IPPROTO_TCP)) == FREERTOS_INVALID_SOCKET) ||
										(FreeRTOS_bind(elisten_sock, &me, sizeof(me)) != 0) ||
										(FreeRTOS_listen(elisten_sock, 4) != 0)) {
											return (NULL);
	}

	/* put listen socket into non-blocking mode
	 * Note: child sockets will inherit these properties as well.*/
	FreeRTOS_setsockopt(elisten_sock,
						0,
						FREERTOS_SO_RCVTIMEO,
						&xReceiveTimeOut,
						sizeof( xReceiveTimeOut ) );
	FreeRTOS_setsockopt(elisten_sock,
						0,
						FREERTOS_SO_SNDTIMEO,
						&xSendTimeOut,
						sizeof( xSendTimeOut ) );

	return (elisten_sock);
}

/* Setup UDP socket for server */
Socket_t
udp_sock_init(uint16_t port){
	struct freertos_sockaddr me;
	const TickType_t xReceiveTimeOut = TCP_RECV_TIMEOUT / portTICK_RATE_MS;
	const TickType_t xSendTimeOut = TCP_SEND_TIMEOUT / portTICK_RATE_MS;
	Socket_t udp_sock;

	//configure socket
	me.sin_family = FREERTOS_AF_INET;
	me.sin_port = FreeRTOS_htons(port);

	udp_sock = FreeRTOS_socket(FREERTOS_AF_INET,
								FREERTOS_SOCK_DGRAM,
								FREERTOS_IPPROTO_UDP);

	//Check that socket was created successfully
	if (udp_sock != FREERTOS_INVALID_SOCKET){
		//Socket created, now bind to port
		if( FreeRTOS_bind( udp_sock, &me, sizeof( &me ) ) == 0 )
		{
			/* The bind was successful. */
		}
		else {
			/* The bind failed*/
			return NULL;
		}
	}
	else {
		//Socket creation failed, possibly due to insufficient memory, or bad options.
		return NULL;
	}

	/* Set the receive timeout. */
	FreeRTOS_setsockopt( udp_sock,            /* The socket being modified. */
						0,                   /* Not used. */
						FREERTOS_SO_RCVTIMEO,/* Setting receive timeout. */
						&xReceiveTimeOut, 	/* The timeout value. */
						0 );                 /* Not used. */

	/* Set the send timeout. */
	FreeRTOS_setsockopt( udp_sock,            /* The socket being modified. */
						0,                   /* Not used. */
						FREERTOS_SO_SNDTIMEO,/* Setting send timeout. */
						&xSendTimeOut,    /* The timeout value. */
						0 );                 /* Not used. */

	//UDP Checksum is on by default, if you want to turn it off, use this
	// Turn the UDP checksum creation off for outgoing UDP packets.
	/*
	FreeRTOS_setsockopt( udp_sock,        // The socket being modified.
						0,               // Not used.
						FREERTOS_SO_UDPCKSUM_OUT, // Setting checksum on/off.
						NULL,            // NULL means off.
						0 );             // Not used.
	*/

	//Done
	return udp_sock;
}

/*
 * Wrapper for tcp_srv_respond() to pass in strings without a length.
 * Use ONLY! for 0-terminated strings!!!
 */
int
tcp_srv_respond_term(Socket_t sock, char *terminated_str)
{
	return tcp_srv_respond(sock, terminated_str, strlen(terminated_str));
}

/* send TCP response to client */
int
tcp_srv_respond(Socket_t sock, char *bufp, int bytestosend)
{
   int bytessent;

   while (bytestosend) {
	  bytessent = FreeRTOS_send(sock,
			  	  	  	  	  	  bufp,
								  bytestosend,
								  0);

      if (bytessent < 0) {
         return (-1);
      } else if (bytessent == 0) {
            return (0);
      } else if (bytessent < bytestosend) {
         /* Only part of the data was written */
         bufp += bytessent;
         bytestosend -= bytessent;
         taskYIELD();       /* Let other tasks run before we try again */
         continue;
      } else {
         break;     /* All bytes sent  */
	  }
   }
   return (0);
}

/* send UDP response to client */
int udp_srv_respond(Socket_t sock, char *bufp, size_t bufsize, struct freertos_sockaddr *dst_address){
	int res;
	res = FreeRTOS_sendto(sock,
							bufp,
							bufsize,
							0,						//Flags, unused, can plumb in if needed in the future
							dst_address,
							sizeof(dst_address));

	return res;
}

bool is_socket_udp(Socket_t sock){
	//FreeRTOS internal socket type, gives access to struct elements.
	FreeRTOS_Socket_t *pxSocket = ( FreeRTOS_Socket_t * ) sock;

	/*Return true if socket is UDP; false otherwise (reasonable assumption is TCP since we're not doing fancy things).*/
	if (pxSocket->ucProtocol == FREERTOS_IPPROTO_UDP){
		return true;
	}
	else {
		// should be FREERTOS_IPPROTO_TCP
		return false;
	}
	return false;
}

int send_network_reply(Socket_t sock, char *bufp, size_t bufsize, struct freertos_sockaddr *dst_address){
	/* send_network_reply is a generalized network transmission wrapper. It will automatically detect if
	 * the input socket is of type UDP or TCP, and act accordingly. If socket type is TCP, then no dest_ipstr,
	 * or dest_port is needed.
	 *
	 * INPUTS:
	 * sock - FreeRTOS socket; can be of UDP or TCP type.
	 * bufp - pointer to data buffer to be transmitted
	 * bufsize - size of buffer to send, including \0 term
	 * dest_address - pointer to struct freertos_sockaddr
	 * 				if NULL and socket is UDP, the default destination is used
	 *
	 * RETURN:
	 * int - return code, 0 indicates success, < 0 indicates failure	*/

	if (is_socket_udp( sock )){

		/* In event dst_address is NULL, use the default */
		if (dst_address){
			return udp_srv_respond(sock, bufp, bufsize, dst_address);
		}
		else {
			return send_udp_broadcast(sock, bufp, bufsize, battery_app.udp_port);
		}
	}
	else {
		return tcp_srv_respond(sock, bufp, bufsize);
	}
	return 0;
}

int send_network_reply_term(Socket_t sock, char *terminated_str, struct freertos_sockaddr *dst_address){
	return send_network_reply(sock, terminated_str, strlen(terminated_str), dst_address);
}

int recv_network_data(Socket_t sock, char *bufp, size_t bufsize, BaseType_t flags, struct freertos_sockaddr *src_address,  socklen_t *src_address_len){
	BaseType_t res;

	/* Determine if incoming data is on TCP or UDP, as this will determine how we reply to said data. */
	if ( is_socket_udp( sock ) ){
		/*Data coming in on UDP socket*/

		res = FreeRTOS_recvfrom(sock,
								bufp,
								bufsize,
								flags,
								src_address,
								src_address_len);

	}
	else {
		/* data arriving on an already-connected TCP socket. */
		res = FreeRTOS_recv(sock,
							bufp,
							bufsize,
							flags);

	}
	return res;
}

int send_udp_broadcast(Socket_t sock, char *bufp, size_t bufsize, int dest_port){
	// Send a UDP broadcast out on port dest_port.
	int res;
	struct freertos_sockaddr address;
	memset(&address, 0, sizeof(address));

	// Broadcasting to the subnet IP address was first attempted by computing the appropriate
	// broadcast given the ip & nm. but for the MBARI public IPs, the packets weren't leaving
	// the interface for some reason. The global broadcast address works however.
	uint32_t broadcast_address = FreeRTOS_inet_addr("255.255.255.255");

	address.sin_family = FREERTOS_AF_INET;
	address.sin_addr = broadcast_address; //FreeRTOS_htonl(broadcast_address);
	address.sin_port = FreeRTOS_htons(dest_port);

	res = FreeRTOS_sendto(sock,
							bufp,
							bufsize,
							0,					//Flags currently unused
							&address,
							sizeof(address));

	return res;
}

int send_udp_broadcast_default(char *bufp, size_t bufsize, int dest_port)
{
	return send_udp_broadcast(default_udp_sock, bufp, bufsize, dest_port);
}

void send_shutdown_warning(int dest_port, uint8_t packs_online, uint8_t sec_remaining)
{
	SHUTDOWN_WARN_T msg;
	msg.num_packs_remaining = packs_online;
	msg.sec_to_shutdown = sec_remaining;
	msg.chksum = checksum8(&msg, sizeof(msg) -1);

	send_udp_broadcast_default( (char *)&msg, sizeof(msg), dest_port);
}

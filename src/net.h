/****************************************************************************/
/* Copyright (c) 2020 MBARI                                                 */
/* MBARI Proprietary Information. All rights reserved.                      */
/****************************************************************************/
/* Summary  :                                                               */
/* Filename : net.h                                                         */
/* Author   :                                                               */
/* Project  :                                                               */
/* Version  : 1.0                                                           */
/* Created  : 2019                                                          */
/* Modified :                                                               */
/* Archived :                                                               */
/****************************************************************************/
/* Modification History:                                                    */
/****************************************************************************/

#ifndef __NET_H_
#define __NET_H_

#include "FreeRTOS.h"
#include "FreeRTOS_Sockets.h"
#include "FreeRTOS_IP.h"
#include "lpc_types.h"

Socket_t tcp_sock_init(uint16_t port);
Socket_t udp_sock_init(uint16_t port);
int send_network_reply(Socket_t sock, char *bufp, size_t bufsize, struct freertos_sockaddr *dst_address);
int send_network_reply_term(Socket_t sock, char *terminated_str, struct freertos_sockaddr *dst_address);
int recv_network_data(Socket_t sock, char*bufp, size_t bufsize, BaseType_t flags, struct freertos_sockaddr *src_address, socklen_t *src_address_len);
int send_udp_broadcast(Socket_t sock, char *bufp, size_t bufsize, int dest_port);
int send_udp_broadcast_default(char *bufp, size_t bufsize, int dest_port);
bool is_socket_udp(Socket_t sock);
int udp_srv_respond(Socket_t, char *bufp, size_t bufsize, struct freertos_sockaddr *dst_address);
int tcp_srv_respond_term(Socket_t sock, char *terminated_str);
int tcp_srv_respond(Socket_t sock, char *bufp, int bytestosend);
void send_shutdown_warning(int dest_port, uint8_t packs_online, uint8_t sec_remaining);

#endif

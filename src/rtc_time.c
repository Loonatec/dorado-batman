

#include "rtc_time.h"
#if defined(ENABLE_RTC_TIME)

/* Useful Constants */
#define SECS_PER_MIN  ((time_t)(60UL))
#define SECS_PER_HOUR ((time_t)(3600UL))
#define SECS_PER_DAY  ((time_t)(SECS_PER_HOUR * 24UL))
static  const uint8_t monthDays[]={31,28,31,30,31,30,31,31,30,31,30,31}; // API starts months from 1, this array starts from 0

// leap year calculator expects year argument as years offset from 1970
#define LEAP_YEAR(Y)     ( ((1970+(Y))>0) && !((1970+(Y))%4) && ( ((1970+(Y))%100) || !((1970+(Y))%400) ) )


RTC_TIME_T FullTime;

void init_RTC() {
	Chip_RTC_Init(LPC_RTC);
	Chip_RTC_Enable(LPC_RTC, ENABLE);
}

int display_RTC_time(char *strPtr) {
	Chip_RTC_GetFullTime(LPC_RTC, &FullTime);

	return sprintf(strPtr, "   RTC: %.2d:%.2d:%.2d %.2d/%.2d/%.4d\r\n",
			FullTime.time[RTC_TIMETYPE_HOUR],
			FullTime.time[RTC_TIMETYPE_MINUTE],
			FullTime.time[RTC_TIMETYPE_SECOND],
			FullTime.time[RTC_TIMETYPE_DAYOFMONTH],
			FullTime.time[RTC_TIMETYPE_MONTH],
			FullTime.time[RTC_TIMETYPE_YEAR]);
}

int set_RTC_time(char *strPtr, char *strTime) {
	int hour, min, sec, day, month, year;
    sscanf(strTime+5 , "%d:%d:%d %d/%d/%d" , &hour, &min, &sec, &day, &month, &year);

    if ( hour > 24 || min > 60 || sec > 60 || day > 31 || month > 12 ) {
    	return sprintf(strPtr, "Time: Invalid format (STIME hh:mm:ss dd/mm/yyyy)\r\n");
    }

	/* Set current time for RTC 2:00:00PM, 2012-10-05 */
	FullTime.time[RTC_TIMETYPE_HOUR]    	= hour;
	FullTime.time[RTC_TIMETYPE_MINUTE]  	= min;
	FullTime.time[RTC_TIMETYPE_SECOND]  	= sec;
	FullTime.time[RTC_TIMETYPE_DAYOFMONTH]  = day;
	FullTime.time[RTC_TIMETYPE_MONTH]   	= month;
	FullTime.time[RTC_TIMETYPE_YEAR]    	= year;

	Chip_RTC_SetFullTime(LPC_RTC, &FullTime);

	return display_RTC_time(strPtr);
}

RTC_TIME_T decodeTime(uint32_t time) {
// note that year is offset from 1970 !!!

	uint8_t year;
	uint8_t month, monthLength;
	unsigned long days;
	RTC_TIME_T timeOutput;

	timeOutput.time[RTC_TIMETYPE_SECOND] = time % 60;
	time /= 60; // now it is minutes
	timeOutput.time[RTC_TIMETYPE_MINUTE] = time % 60;
	time /= 60; // now it is hours
	timeOutput.time[RTC_TIMETYPE_HOUR] = time % 24;
	time /= 24; // now it is days

	year = 0;
	days = 0;
	while((unsigned)(days += (LEAP_YEAR(year) ? 366 : 365)) <= time) {
		year++;
	}
	timeOutput.time[RTC_TIMETYPE_YEAR] = year + 1970; // year is offset from 1970

	days -= LEAP_YEAR(year) ? 366 : 365;
	time  -= days; // now it is days in this year, starting at 0

	days=0;
	month=0;
	monthLength=0;
	for (month=0; month<12; month++) {
		if (month==1) { // february
			if (LEAP_YEAR(year)) { monthLength=29; }
			else { monthLength=28; }
		} else {
			monthLength = monthDays[month];
		}

		if (time >= monthLength) {
			time -= monthLength;
		} else { break; }
	}
	timeOutput.time[RTC_TIMETYPE_MONTH] = month + 1;  // jan is month 1
	timeOutput.time[RTC_TIMETYPE_DAYOFMONTH] = time + 1;     // day of month

	return timeOutput;
}


uint32_t encodeTime(){
	int i, offset_year;
	uint32_t seconds;

	Chip_RTC_GetFullTime(LPC_RTC, &FullTime);
	offset_year = FullTime.time[RTC_TIMETYPE_YEAR]-1970;	// year is offset from 1970

	// seconds from 1970 till 1 jan 00:00:00 of the given year
	seconds = (offset_year)*(SECS_PER_DAY * 365);
	for (i = 0; i < offset_year; i++) {
		if (LEAP_YEAR(i)) {
			seconds += SECS_PER_DAY;   // add extra days for leap years
		}
	}

	// add days for this year, months start from 1
	for (i = 1; i < FullTime.time[RTC_TIMETYPE_MONTH]; i++) {
		if ( (i == 2) && LEAP_YEAR(offset_year)) {
			seconds += SECS_PER_DAY * 29;
		} else {
			seconds += SECS_PER_DAY * monthDays[i-1];  //monthDay array starts from 0
		}
	}
	seconds+= (FullTime.time[RTC_TIMETYPE_DAYOFMONTH]-1) * SECS_PER_DAY;
	seconds+= FullTime.time[RTC_TIMETYPE_HOUR] * SECS_PER_HOUR;
	seconds+= FullTime.time[RTC_TIMETYPE_MINUTE] * SECS_PER_MIN;
	seconds+= FullTime.time[RTC_TIMETYPE_SECOND];
	return (time_t)seconds;
}

# else

void init_RTC() {}

int display_RTC_time(char *strPtr) {
	return sprintf(strPtr, "RTC Disabled");
}

int set_RTC_time(char *strPtr, char *strTime) {
	return sprintf(strPtr, "RTC Disabled");
}

RTC_TIME_T decodeTime(uint32_t time) { return 0; }
uint32_t encodeTime(){ return 0; }

#endif

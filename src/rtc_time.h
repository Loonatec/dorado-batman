/****************************************************************************/
/* Summary  : RTC Peripheral implementation for timestamping data log       */
/* Filename : time.h                                                        */
/* Author   : Jedediah Smith (j.smith_at_loonatec.com)                      */
/* Project  :                                                               */
/* Version  : 1.0                                                           */
/* Created  : Feb 2021                                                      */
/* Modified :                                                               */
/* Archived :                                                               */
/****************************************************************************/
/* Modification History:                                                    */
/****************************************************************************/

/* decode/encodeTime() is based on: https://github.com/PaulStoffregen/Time */

#ifndef RTC_TIME_H_
#define RTC_TIME_H_

#define ENABLE_RTC_TIME		// Uncomment to enable the use of the internal RTC peripheral

#include "board.h"

/**
 * \brief Enables the RTC peripheral
 */
void init_RTC();

/**
 * \brief Creates a formatted text string displaying current time
 *
 * \return int : number of bytes written to string
 */
int display_RTC_time(char *strPtr);

/**
 * \brief Sets the RTC from an input string
 *
 * \return int : number of bytes written to string
 */
int set_RTC_time(char *strPtr, char *strTime);

/**
 * \brief Decodes Linux time into RTC format for displaying
 *
 * \return RTC_TIME_T : same format as RTC time uses
 */
RTC_TIME_T decodeTime(uint32_t time);

/**
 * \brief Encode the current RTC time into Linux time
 *
 * \return uint32_t : number of seconds since 1970
 */
uint32_t encodeTime();

#endif /* RTC_TIME_H_ */

/*
 * sensors.c
 *
 *  Created on: Mar 10, 2021
 *      Author: Jedediah Smith (j.smith@loonatec.com)
 */

#include "sensors.h"

#include "smbus.h"
#include "utils.h"
#include "external_eeprom.h"

// MS5637 device commands
#define MS5637_RESET_COMMAND										0x1E
#define MS5637_START_PRESSURE_ADC_CONVERSION						0x40
#define MS5637_START_TEMPERATURE_ADC_CONVERSION						0x50
#define MS5637_READ_ADC												0x00
#define MS5637_PROM_ADDRESS_READ_ADDRESS_0							0xA0

#define MS5637_CONVERSION_OSR_MASK									0x0F

#define MS5637_CONVERSION_TIME_OSR_256								1000
#define MS5637_CONVERSION_TIME_OSR_512								2000
#define MS5637_CONVERSION_TIME_OSR_1024								3000
#define MS5637_CONVERSION_TIME_OSR_2048								5000
#define MS5637_CONVERSION_TIME_OSR_4096								9000
#define MS5637_CONVERSION_TIME_OSR_8192								17000

// Coefficients indexes for temperature and pressure computation
#define MS5637_CRC_INDEX											0
#define MS5637_PRESSURE_SENSITIVITY_INDEX							1
#define MS5637_PRESSURE_OFFSET_INDEX								2
#define MS5637_TEMP_COEFF_OF_PRESSURE_SENSITIVITY_INDEX				3
#define MS5637_TEMP_COEFF_OF_PRESSURE_OFFSET_INDEX					4
#define MS5637_REFERENCE_TEMPERATURE_INDEX							5
#define MS5637_TEMP_COEFF_OF_TEMPERATURE_INDEX						6
#define MS5637_COEFFICIENT_NUMBERS									7

#define MAX_TEMP_TO_LOG		45
#define MAX_HUM_TO_LOG		75
#define MAX_PRES_TO_LOG		800

// --- Static functions ---
static void tmp112_read(void);
static void hih6130_read(void);
static bool ms5637_is_connected(void);
static enum ms5637_status ms5637_reset(void);
static enum ms5637_status ms5637_write_command(uint8_t);
static enum ms5637_status ms5637_read_eeprom(void);
static enum ms5637_status ms5637_conversion_and_read_adc( uint8_t, uint32_t *);
static enum ms5637_status ms5637_read( int *, int *);
static bool ms5637_crc_check (uint16_t *);

static uint16_t TMP112_Temp;
static uint32_t HIH6130_Temp;
static uint16_t HIH6130_Hum;
static int MS5637_Temp;
static int MS5637_Press;

static bool ms5637_enabled;
static enum ms5637_resolution_osr ms5637_resolution_osr;
static uint16_t eeprom_coeff[8];
static bool ms5637_coeff_read = false;

static const uint8_t TMP112_ADDR  = 0x90>>1; //Shifted address for the TMP112
static const uint8_t HIH6130_ADDR = 0x4E>>1; //Shifted address for the HIH6130
static const uint8_t MS5637_ADDR  = 0x76; //Shifted address for the MS5637


// ----- Public Functions ----- //
void init_sensors(void) {
	// To configure the Humidity Alarm LED: https://phanderson.com/arduino/UsingAlarms.pdf
	// Note: can only be done in the first 3 ms after power-on. See: https://sensing.honeywell.com/command-mode-humidicon-tn-009062-3-en-final-07jun12.pdf

	/* Note the MS5637 has a 7-bit I2C address of 0x76 which is the same as the 6-cell mux chip */

	if (!setSmbusMux(5, 7)) {
		inc_totalSMBusErrors();
	} else {
		ms5637_resolution_osr = ms5637_resolution_osr_4096;
		ms5637_enabled = ms5637_is_connected();
		if (ms5637_enabled) { ms5637_read((int*)&MS5637_Temp, (int*)&MS5637_Press); } // get coeff's
	}
}

void read_sensors() {
	if (!setSmbusMux(5, 1)) {
		inc_totalSMBusErrors();
	} else {
		tmp112_read();
		if (get_TMP112_Temp_C() > MAX_TEMP_TO_LOG) { log_sensor_event(sensor_temperature_high, temperature_sensor); }
	}

	if (!setSmbusMux(5, 7)) {
		inc_totalSMBusErrors();
	} else {
		hih6130_read();
		if (get_HIH6130_Temp_C() > MAX_TEMP_TO_LOG) { log_sensor_event(sensor_temperature_high, humidity_sensor); }
		if (get_HIH6130_Hum_p() > MAX_HUM_TO_LOG) { log_sensor_event(sensor_humidity_high, humidity_sensor); }

		if (ms5637_enabled) {
			ms5637_read((int*)&MS5637_Temp, (int*)&MS5637_Press);
			if (get_MS5637_Temp_C() > MAX_TEMP_TO_LOG) { log_sensor_event(sensor_temperature_high, pressure_sensor); }
			if (get_MS5637_Press_hPa() > MAX_PRES_TO_LOG) { log_sensor_event(sensor_pressure_high, pressure_sensor); }
		}
	}
}

/** Formatted text string to display sensor information */
int display_sensors(char * strPtr) {
	char tempStr[20];

	if (get_ms5637_enabled()) { sprintf(tempStr, "%4.1f hPa", get_MS5637_Press_hPa()); }
	else { sprintf(tempStr, "Disabled"); }

	return sprintf(strPtr, \
			"\r\n-- Ambient Sensor Data --\r\n" \
			"   Temperature: %3.1f C\r\n" \
			"   Humidity:    %3.1f %%RH\r\n" \
			"   Pressure:    %s\r\n",
			get_TMP112_Temp_C(),
			get_HIH6130_Hum_p(),
			tempStr
	);
}

float get_TMP112_Temp_C() {
	return TMP112_Temp  * 0.0625F;
}

float get_HIH6130_Temp_C() {
	return HIH6130_Temp * 0.010072F - 40;
}

float get_HIH6130_Hum_p() {
	return HIH6130_Hum * 0.006104F;
}

float get_MS5637_Temp_C(void) {
	return MS5637_Temp / 100.0;
}

float get_MS5637_Press_hPa(void) {
	return MS5637_Press / 10.0;
}

bool get_ms5637_enabled(void) {
	return ms5637_enabled;
}



// ----- Private Functions ---- //
void tmp112_read(void) {
	(void) smbus_read_reg((TMP112_ADDR), 0x00, (uint8_t *)&TMP112_Temp, 2, false);
	TMP112_Temp = ((TMP112_Temp&0x00ff)<<8) | ((TMP112_Temp&0xff00)>>8);
	TMP112_Temp >>= 4;
}

static void hih6130_read(void) {
	/* Start humidity measurement */
	Chip_I2C_MasterSend(I2C0, (HIH6130_ADDR), (uint8_t *)&HIH6130_Temp, 1);
	vTaskDelay(50 / portTICK_RATE_MS);

	/* Read humidity measurement */
//	(void) smbus_read_reg(I2C0, (HIH6130_ADDR), (uint8_t *)&HIH6130_Temp, 4, false);
	Chip_I2C_MasterRead(I2C0, (0x4e>>1), (uint8_t *)&HIH6130_Temp, 4);

	/* Calculate Values */
	HIH6130_Hum = HIH6130_Temp & 0x0000ffff;
	HIH6130_Hum = ((HIH6130_Hum & 0x00ff)<<8) | ((HIH6130_Hum & 0xff00)>>8);
	HIH6130_Temp = HIH6130_Temp>>16;
	HIH6130_Temp = ((HIH6130_Temp & 0x00ff)<<8) | ((HIH6130_Temp & 0xff00)>>8);
	HIH6130_Temp >>= 2;
}

/** MS5637 - Check whether MS5637 device is connected */
bool ms5637_is_connected(void) {
	if ( Chip_I2C_MasterSend(I2C0, MS5637_ADDR, NULL, 1) == 1) { return true; }
	return false;
}

/** MS5637 - Reset the MS5637 device */
enum ms5637_status  ms5637_reset(void) {
	return ms5637_write_command(MS5637_RESET_COMMAND);
}

/** MS5637 - Writes the MS5637 8-bits command with the value passed */
enum ms5637_status ms5637_write_command( uint8_t cmd) {
	uint8_t data[1];
	data[0] = cmd;

	/* Do the transfer */
	if ( Chip_I2C_MasterSend(I2C0, MS5637_ADDR, data, 1) != 1 ) {
		return ms5637_status_no_i2c_acknowledge;
	}

	return ms5637_status_ok;
}

/** MS5637 - Reads the ms5637 EEPROM coefficients to store them for computation */
enum ms5637_status ms5637_read_eeprom(void) {
	uint8_t i, buffer[2];

	setSmbusMux(5, 7);
	ms5637_reset();

	for( i=0 ; i< MS5637_COEFFICIENT_NUMBERS ; i++) {
		buffer[0] = 0;
		buffer[1] = 0;

		Chip_I2C_MasterCmdRead(I2C0, 0x76, (0xA0 + i*2), buffer, 2);
		if ( buffer[0] == 0 && buffer[1] == 0 ) { return ms5637_status_i2c_transfer_error; }

		eeprom_coeff[i] = (buffer[0] << 8) | buffer[1];
	}

	if( !ms5637_crc_check( eeprom_coeff )) { return ms5637_status_crc_error; }

	ms5637_coeff_read = true;
	return ms5637_status_ok;
}

/** MS5637 - Triggers conversion and read ADC value */
static enum ms5637_status ms5637_conversion_and_read_adc(uint8_t cmd, uint32_t *adc) {
	enum ms5637_status status;
	uint8_t buffer[3];

	buffer[0] = 0;
	buffer[1] = 0;
	buffer[2] = 0;

	/* Start Measurement */
	status = ms5637_write_command(cmd);
	if( status != ms5637_status_ok) { return status; }

	// delay conversion depending on resolution
	vTaskDelay(10 / portTICK_RATE_MS);

	// Read Results
	if (Chip_I2C_MasterCmdRead(I2C0, MS5637_ADDR, MS5637_READ_ADC, buffer, 3) != 3) {
		return ms5637_status_i2c_transfer_error;
	}

	*adc = ((uint32_t)buffer[0] << 16) | ((uint32_t)buffer[1] << 8) | buffer[2];

	return status;
}

/** MS5637 - Reads the temperature and pressure ADC value and compute the compensated values. */
enum ms5637_status ms5637_read( int *temperature, int *pressure) {
	enum ms5637_status status = ms5637_status_ok;
	uint32_t adc_temperature, adc_pressure;
	int32_t dT, TEMP;
	int64_t OFF, SENS, P, T2, OFF2, SENS2;
	uint8_t cmd;

	// If first time adc is requested, get EEPROM coefficients
	if( ms5637_coeff_read == false )
		status = ms5637_read_eeprom();
	if( status != ms5637_status_ok)
		return status;

	// First read temperature
	cmd = ms5637_resolution_osr*2;
	cmd |= MS5637_START_TEMPERATURE_ADC_CONVERSION;
	status = ms5637_conversion_and_read_adc( cmd, &adc_temperature);
	if( status != ms5637_status_ok)
		return status;

	// Now read pressure
	cmd = ms5637_resolution_osr*2;
	cmd |= MS5637_START_PRESSURE_ADC_CONVERSION;
	status = ms5637_conversion_and_read_adc( cmd, &adc_pressure);
	if( status != ms5637_status_ok)
		return status;

    if (adc_temperature == 0 || adc_pressure == 0) { return ms5637_status_i2c_transfer_error; }

	// Difference between actual and reference temperature = D2 - Tref
	dT = (int32_t)adc_temperature - ((int32_t)eeprom_coeff[MS5637_REFERENCE_TEMPERATURE_INDEX] <<8 );

	// Actual temperature = 2000 + dT * TEMPSENS
	TEMP = 2000 + ((int64_t)dT * (int64_t)eeprom_coeff[MS5637_TEMP_COEFF_OF_TEMPERATURE_INDEX] >> 23) ;

	// Second order temperature compensation
	if( TEMP < 2000 ) 	{
		T2 = ( 3 * ( (int64_t)dT  * (int64_t)dT  ) ) >> 33;
		OFF2 = 61 * ((int64_t)TEMP - 2000) * ((int64_t)TEMP - 2000) / 16 ;
		SENS2 = 29 * ((int64_t)TEMP - 2000) * ((int64_t)TEMP - 2000) / 16 ;

		if( TEMP < -1500 ) 		{
			OFF2 += 17 * ((int64_t)TEMP + 1500) * ((int64_t)TEMP + 1500) ;
			SENS2 += 9 * ((int64_t)TEMP + 1500) * ((int64_t)TEMP + 1500) ;
		}
	} else 	{
		T2 = ( 5 * ( (int64_t)dT  * (int64_t)dT  ) ) >> 38;
		OFF2 = 0 ;
		SENS2 = 0 ;
	}

	// OFF = OFF_T1 + TCO * dT
	OFF = ( (int64_t)(eeprom_coeff[MS5637_PRESSURE_OFFSET_INDEX]) << 17 ) + ( ( (int64_t)(eeprom_coeff[MS5637_TEMP_COEFF_OF_PRESSURE_OFFSET_INDEX]) * dT ) >> 6 ) ;
	OFF -= OFF2 ;

	// Sensitivity at actual temperature = SENS_T1 + TCS * dT
	SENS = ( (int64_t)eeprom_coeff[MS5637_PRESSURE_SENSITIVITY_INDEX] << 16 ) + ( ((int64_t)eeprom_coeff[MS5637_TEMP_COEFF_OF_PRESSURE_SENSITIVITY_INDEX] * dT) >> 7 ) ;
	SENS -= SENS2 ;

	// Temperature compensated pressure = D1 * SENS - OFF
	P = ( ( (adc_pressure * SENS) >> 21 ) - OFF ) >> 15 ;

	*temperature =TEMP - T2;
	*pressure = P / 10;

	return status;
}

/** MS5637 - 4-bit CRC check */
bool ms5637_crc_check (uint16_t *n_prom) {
    uint8_t cnt;
    int8_t n_bit;
    uint16_t n_rem = 0;
    uint16_t crc_init = (eeprom_coeff[MS5637_CRC_INDEX] & 0xF000 ) >> 12;

    n_prom[MS5637_CRC_INDEX]=((n_prom[MS5637_CRC_INDEX])&0x0FFF);		// CRC byte is replaced by 0
    n_prom[7]=0; 														// Subsidiary value, set to 0
    for(cnt=0;cnt<16;cnt++) {											// Operation is performed on bytes, choose LSB or MSB
    	if (cnt%2==1) { n_rem^=(unsigned short)((n_prom[cnt>>1])&0x00FF); }
    	else { n_rem^=(unsigned short)(n_prom[cnt>>1]>>8); }
    	for(n_bit=8;n_bit>0;n_bit--) {
    		if (n_rem&(0x8000)) {n_rem=(n_rem<<1)^0x3000; }
    		else {n_rem=(n_rem<<1);}
    	}
    }
    n_rem = ( (n_rem>>12) & 0x000F );					// final 4-bit reminder is CRC code

    return( (n_rem^0x00) == crc_init );
}

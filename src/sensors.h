/*
 * sensors.h
 *
 *  Created on: Mar 10, 2021
 *      Author: Jedediah Smith (j.smith@loonatec.com)
 *
 *  MS5637 Code based on datasheet and TE Github repository (https://github.com/TEConnectivity/MS5637_Generic_C_Driver)
 *    	Copyright (c) 2016 Measurement Specialties. All rights reserved.
 *    	MIT License

		Copyright (c) 2016 TE Connectivity

		Permission is hereby granted, free of charge, to any person obtaining a copy
		of this software and associated documentation files (the "Software"), to deal
		in the Software without restriction, including without limitation the rights
		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
		copies of the Software, and to permit persons to whom the Software is
		furnished to do so, subject to the following conditions:

		The above copyright notice and this permission notice shall be included in all
		copies or substantial portions of the Software.

		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
		SOFTWARE.
 */

#ifndef SENSORS_H_
#define SENSORS_H_

#include "board.h"
#include "chip.h"

#include <stdint.h>
#include <stdbool.h>
#include <math.h>

enum ms5637_resolution_osr {
	ms5637_resolution_osr_256 = 0,
	ms5637_resolution_osr_512,
	ms5637_resolution_osr_1024,
	ms5637_resolution_osr_2048,
	ms5637_resolution_osr_4096,
	ms5637_resolution_osr_8192
};

enum ms5637_status {
	ms5637_status_ok,
	ms5637_status_no_i2c_acknowledge,
	ms5637_status_i2c_transfer_error,
	ms5637_status_crc_error
};

/**
 * \brief Check whether MS5637 device is connected
 */
void init_sensors(void);

/**
 * \brief Requests and reads the latest data from the sensors
 */
void read_sensors(void);

/**
 * \brief Formatted text string to display sensor information
 *
 * \return int : number of chars written to string
 */
int display_sensors(char *);

/**
 * \brief Returns the last read temperature value from the TMP112 module
 *
 * \return float : last read temperature value, in degrees C
 */
float get_TMP112_Temp_C(void);

/**
 * \brief Returns the last read temperature value from the HIH6130 module
 *
 * \return float : last read temperature value, in degrees C
 */
float get_HIH6130_Temp_C(void);

/**
 * \brief Returns the last read humidity value from the TMP112 module
 *
 * \return float : last read humidity value, in percent relative humidity
 */
float get_HIH6130_Hum_p(void);

/**
 * \brief Returns the last read temperature value from the MS5637 module
 *
 * \return float : last read temperature value, in degrees C
 */
float get_MS5637_Temp_C(void);

/**
 * \brief Returns the last read pressure value from the MS5637 module
 *
 * \return float : last read pressure value, hPa (aka mBar)
 */
float get_MS5637_Press_hPa(void);

/**
 * \brief Returns the enabled state of the MS5637
 *
 * \return bool : enabled state of the MS5637
 */
bool get_ms5637_enabled(void);

#endif /* SENSORS_H_ */

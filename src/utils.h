/****************************************************************************/
/* Copyright (c) 2020 MBARI                                                 */
/* MBARI Proprietary Information. All rights reserved.                      */
/****************************************************************************/
/* Summary  :                                                               */
/* Filename : utils.h                                                       */
/* Author   :                                                               */
/* Project  :                                                               */
/* Version  : 1.0                                                           */
/* Created  : 2019                                                          */
/* Modified :                                                               */
/* Archived :                                                               */
/****************************************************************************/
/* Modification History:                                                    */
/****************************************************************************/

#include "stdint.h"
#include "FreeRTOS.h"
#include "FreeRTOS_Sockets.h"

void utils_task_sleep(uint32_t ms);
void utils_block_sleep(uint32_t ms);
void utils_socket_close(Socket_t socket);



#define utils_addr_to_array(value)						\
	(													\
		(uint8_t *)FreeRTOS_htonl((uint32_t)value)		\
	)
	
#define utils_str_addr_to_array(value)					\
	(													\
		utils_addr_to_array(FreeRTOS_inet_addr(value))	\
	)

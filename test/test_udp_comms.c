/*
 * Simple query program to test UDP comms to MBARI Battery balls.
 * Laughlin Barker | October 2019
 *
 */
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
//#include <unistd.h>
#include <string.h>
#include "udp_types.h"

#define PORT 20003
const char battery_ip[16] = "134.89.32.133";

#define BUFF_SIZE 1500
char buff[BUFF_SIZE];

char pack_types[3][5] = {"NA", "29", "34"};
char pack_states[4][6] = {"OFF", "ON", "SHTDN", "NA"};

void print_ball_data(BALL_DATA_T *data){

	//calculate number of battery backs (should be hard coded in udp_types.h)
	int num_batts = sizeof(data->pack)/sizeof(data->pack[0]);

	//Cycle through all
	for (int num = 0; num < num_batts; num++){
		uint8_t state = (data->pack[num].aux_data_bits & (0b11 << STATE_LSB)) >> STATE_LSB;
		uint8_t batt_type = (data->pack[num].aux_data_bits & (0b11 << TYPE_LSB)) >> TYPE_LSB;
		printf("Pack %02d:  %04d mV  %04d mA  %03d C  %05d mAh  status: %04X  state: %s  type: %s \r\n",
				num,
				data->pack[num].volts_mV,
				data->pack[num].current_mA,
				data->pack[num].temp_Cx10,
				data->pack[num].remaining_capacity_mAh,
				data->pack[num].status,
				pack_states[state],
				pack_types[batt_type]);
	}
	printf("\n");
	printf("Relative Humidity: %d %% \n", data->relhum_pct);
}

int main(int argc, char const *argv[])
{
	const char request_cmd[7] = "@99UR\r\n";
	int sock = 0, bytes_read, bytes_sent;
	struct sockaddr_in battery_addr;
	socklen_t len = sizeof(battery_addr);
	BALL_DATA_T *data;

    //Ensure buffer zeroed out
    memset(&buff, 0, sizeof(buff));

    //create socket
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0)
    {
        printf("\n Socket creation error \n");
        return -1;
    }

    battery_addr.sin_family = AF_INET;
    battery_addr.sin_port = htons(PORT);

    // Convert IPv4 and IPv6 addresses from text to binary form
    if(inet_pton(AF_INET, battery_ip, &battery_addr.sin_addr)<=0)
    {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    //Send request
    bytes_sent = sendto(sock ,
    					(const char *)request_cmd ,
						strlen(request_cmd) ,
						0,
						(const struct sockaddr *)&battery_addr,
						len);

    printf("Sent %d byte request\n", bytes_sent);

    //Read back data
    bytes_read = recvfrom(sock ,
    						&buff,
							BUFF_SIZE,
							0,
							(struct sockaddr *)&battery_addr,
							&len);

    printf("Read %d bytes. \n",bytes_read);

    //Cast network buffer pointer to our battery struct pointer type
    data = (BALL_DATA_T *)buff;

    print_ball_data(data);

    memset(&buff, 0, sizeof(buff));


    printf("Exiting!\n");
    return 0;
}
